#! /usr/bin/env python
#
#  Version for Run3 Symmetric layout
#  NSWA+NSWC installed
#  EIL1, EIL3, EIL3, EIS1, EIS2, BIS8A removed
#  BIS7A converted to sMDT, BIS7C, BIS8C stay as MDT
#  1098 MDT+sMDT chambers
#
# A structure listing all chamber names and their geometry parameters
# many routines to access them and to have an easy mapping
#  
#  Use like this:
# import chamberlist
# idx=chamberlist.MDTindex("BIL1A01")
#
#  List of methods.  Generally these return -1 to indicate failure
#  CheckChamber( chamber, ml, layer, tube )   => Check for valid
#                   chamber name. Returns mdt[] index of chamber
#  MDTindex( ChName/muonfixedid ) => Returns index in mdt[] for ChName or muonfixedid
#  MDTmfid( ChName, ml=1, ly=1, tb=1 ) => Returns muonfixedid for ChName. 
#    or MDTmfid( ChName, mllt ) => Returns muonfixedid for ChName. 
#  MDTcheckMfid( mfid )     => Returns MDTindex if mfid is valid; -1 if not
#  MDThardname( idx )       => Returns hardware/online name from idx
#              (idx can be mdt[] index, calname, or muonfixedid)
#  MDTcalname( idx )       => Returns calname/offline name from idx
#              (idx can be mdt[] index, hardname, or muonfixedid)
#  MDTtubename( muonfixedid, chet ) => Returns hardware name-ML-L-T from a muonfixedid (if chet=1 then use calname rather than hardname)
#  MDTnML(  ChName )        => Returns number of ML in the chamber
#  MDTtotalLayers( ChName ) => Returns number of Layers in the chamber
#  MDTtotalTubes( ChName )  => Returns TOT number of Tubes  in the chamber
#  MDTnmezz( ChName )       => Returns number of Mezzanines in the chamber
#  MDTmaxmezz( ChName )     => Returns number of mezzcards included skipped mezzcards due to cut outs
#  MDTtypeML( ChName, ml )  => Returns the ML MEZZ type (1-4) 
#  MDTtypeMZ( ChName, mz )  => Returns the MZ MEZZ type (1-4)
#  MDTmlMZ0( ChName )       => Returns ML number where there is Mezz number 0
#  MDTmlMZ( ChName, mz )    => Returns ML number of Mezz number mz
#  MDTnLml( ChName, ml )    => Returns number of Layers in the ML ml=0=both ML
#  MDTnTly( ChName, ml )    => Returns number of Tubes/Layer in the ML  ml=0=both ML
#  MDTnTml( ChName, ml )    => Returns number of Tubes in the ML
#  MLLT2MLLT( ChName, mllt) => Convert MLLT to ml, ly, tb; Returns tuple ml, ly, tb
#                              MLLT = 1000*ml + 100*ly + tb OR 10000*ml + 1000*ly + tb (BIS7A)
#  T2MLLT( ChName, tb )     => tubeNumber to MLLT  tubeNumber=[1..<total tubes>]
#  MLLT2T( ChName, mllt )   => MLLT to tubeNumber  [1..<total tubes>]
#  MLLT2L( ChName, mllt )   => MLLT to layerNumber [1..<total layers>]
#  MLLT2M( ChName, mllt )   => MLLT to mezzNumber  [0.. ]
#  MLLT2M( ChName, mllt )   => tubeNumber to mezzNumber
#  MLLT2MezzCh( cham,mllt ) => From cham,mllt return tuple of mezzcard type, mezzcard channel 
#                              => DOES NOT WORK FOR BME, BMG, for those use mezzcardmap.py
#  MDTtubelength( cham )    => Returns max tube length in chamber [mm]
#  getdate(date)            => Convert date from DD-<Month>-YYYY to YYYYMMDD
#  sMDTlist()               => Returns a list of idx numbers for sMDT chambers
#
#  Number of mezzcards per ML
#  ntl_ml1(2)/24/nly_ml1(2) or ntl_ml1(2)/(mzt_ml1(2)<3?8:6)
import sys,muonfixedid

version = "Run3_Symmetric"
#print("chamberlist version",version)

class ChInfo:
  "Info for a single MDT chamber"

# init class
  def __init__(self,calname,hardname,eta,phi,station,num_ml,nly_ml1,ntl_ml1,nly_ml2,ntl_ml2,n_mezz,mz0ml,mzt_ml1,mzt_ml2,max_tube_len,MDTtype):
    self.calname  = calname      #software/offline name e.g. BIL_1_1
    self.hardname = hardname     #hardware/online name e.g. BIL1A01
    self.eta      = eta          #chamber eta; +=>A, -=>C
    self.phi      = phi          #phi [1..8]
    self.station  = station      #3-letter station name, e.g. BIL
    self.num_ml   = num_ml       #number of ML
    self.nly_ml1  = nly_ml1      #number of layers in ML1
    self.ntl_ml1  = ntl_ml1      #number of tubes/layer in ML1
    self.nly_ml2  = nly_ml2      #number of layers in ML2 
    self.ntl_ml2  = ntl_ml2      #number of tubes/layer in ML2
    self.n_mezz   = n_mezz       #number of mezz cards (actual mezzcards, does not include non-existant mezzcards in cutouts)
    self.mz0ml    = mz0ml        #number of ML with mezzcard 0 (mezzcards numbered by CSM channel)
    self.mzt_ml1  = mzt_ml1      #ML1 mezzcard type (1 and 2 are 3x8, 3 and 4 4x6)
    self.mzt_ml2  = mzt_ml2      #ML2 mezzcard type
    self.max_tube_len = max_tube_len   #maximum tube length [mm]
    self.MDTtype  = MDTtype      #flag for MDT=0; sMDT=1


###  List of all chambers and geometry parameters
mdt = [
  ChInfo("BEE_1_1" ,  "BEE1A02",  1, 1, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   0
  ChInfo("BEE_2_1" ,  "BEE1A04",  1, 2, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   1
  ChInfo("BEE_3_1" ,  "BEE1A06",  1, 3, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   2
  ChInfo("BEE_4_1" ,  "BEE1A08",  1, 4, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   3
  ChInfo("BEE_5_1" ,  "BEE1A10",  1, 5, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   4
  ChInfo("BEE_6_1" ,  "BEE1A12",  1, 6, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   5
  ChInfo("BEE_7_1" ,  "BEE1A14",  1, 7, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   6
  ChInfo("BEE_8_1" ,  "BEE1A16",  1, 8, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #   7
  ChInfo("BEE_1_-1",  "BEE1C02", -1, 1, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #   8
  ChInfo("BEE_2_-1",  "BEE1C04", -1, 2, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #   9
  ChInfo("BEE_3_-1",  "BEE1C06", -1, 3, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  10
  ChInfo("BEE_4_-1",  "BEE1C08", -1, 4, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  11
  ChInfo("BEE_5_-1",  "BEE1C10", -1, 5, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  12
  ChInfo("BEE_6_-1",  "BEE1C12", -1, 6, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  13
  ChInfo("BEE_7_-1",  "BEE1C14", -1, 7, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  14
  ChInfo("BEE_8_-1",  "BEE1C16", -1, 8, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  15
  ChInfo("BEE_1_2" ,  "BEE2A02",  2, 1, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  16
  ChInfo("BEE_2_2" ,  "BEE2A04",  2, 2, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  17
  ChInfo("BEE_3_2" ,  "BEE2A06",  2, 3, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  18
  ChInfo("BEE_4_2" ,  "BEE2A08",  2, 4, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  19
  ChInfo("BEE_5_2" ,  "BEE2A10",  2, 5, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  20
  ChInfo("BEE_6_2" ,  "BEE2A12",  2, 6, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  21
  ChInfo("BEE_7_2" ,  "BEE2A14",  2, 7, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  22
  ChInfo("BEE_8_2" ,  "BEE2A16",  2, 8, "BEE", 1, 4, 48, 0,  0,  8, 1, 3, 0,  912, 0),   #  23
  ChInfo("BEE_1_-2",  "BEE2C02", -2, 1, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  24
  ChInfo("BEE_2_-2",  "BEE2C04", -2, 2, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  25
  ChInfo("BEE_3_-2",  "BEE2C06", -2, 3, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  26
  ChInfo("BEE_4_-2",  "BEE2C08", -2, 4, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  27
  ChInfo("BEE_5_-2",  "BEE2C10", -2, 5, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  28
  ChInfo("BEE_6_-2",  "BEE2C12", -2, 6, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  29
  ChInfo("BEE_7_-2",  "BEE2C14", -2, 7, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  30
  ChInfo("BEE_8_-2",  "BEE2C16", -2, 8, "BEE", 1, 4, 48, 0,  0,  8, 1, 4, 0,  912, 0),   #  31
  ChInfo("BIL_1_1" ,  "BIL1A01",  1, 1, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  32
  ChInfo("BIL_2_1" ,  "BIL1A03",  1, 2, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  33
  ChInfo("BIL_3_1" ,  "BIL1A05",  1, 3, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  34
  ChInfo("BIL_4_1" ,  "BIL1A07",  1, 4, "BIL", 2, 4, 24, 4, 24,  8, 1, 4, 3, 2672, 0),   #  35
  ChInfo("BIL_5_1" ,  "BIL1A09",  1, 5, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  36
  ChInfo("BIL_7_1" ,  "BIL1A13",  1, 7, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  37
  ChInfo("BIL_1_-1",  "BIL1C01", -1, 1, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  38
  ChInfo("BIL_2_-1",  "BIL1C03", -1, 2, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  39
  ChInfo("BIL_3_-1",  "BIL1C05", -1, 3, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  40
  ChInfo("BIL_4_-1",  "BIL1C07", -1, 4, "BIL", 2, 4, 24, 4, 24, 8 , 2, 3, 4, 2672, 0),   #  41
  ChInfo("BIL_5_-1",  "BIL1C09", -1, 5, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  42
  ChInfo("BIL_7_-1",  "BIL1C13", -1, 7, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  43
  ChInfo("BIL_1_2" ,  "BIL2A01",  2, 1, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  44
  ChInfo("BIL_2_2" ,  "BIL2A03",  2, 2, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  45
  ChInfo("BIL_3_2" ,  "BIL2A05",  2, 3, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  46
  ChInfo("BIL_4_2" ,  "BIL2A07",  2, 4, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  47
  ChInfo("BIL_5_2" ,  "BIL2A09",  2, 5, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  48
  ChInfo("BIL_7_2" ,  "BIL2A13",  2, 7, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  49
  ChInfo("BIL_1_-2",  "BIL2C01", -2, 1, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  50
  ChInfo("BIL_2_-2",  "BIL2C03", -2, 2, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  51
  ChInfo("BIL_3_-2",  "BIL2C05", -2, 3, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  52
  ChInfo("BIL_4_-2",  "BIL2C07", -2, 4, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  53
  ChInfo("BIL_5_-2",  "BIL2C09", -2, 5, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  54
  ChInfo("BIL_7_-2",  "BIL2C13", -2, 7, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  55
  ChInfo("BIL_1_3" ,  "BIL3A01",  3, 1, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  56
  ChInfo("BIL_2_3" ,  "BIL3A03",  3, 2, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  57
  ChInfo("BIL_3_3" ,  "BIL3A05",  3, 3, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  58
  ChInfo("BIL_4_3" ,  "BIL3A07",  3, 4, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  59
  ChInfo("BIL_5_3" ,  "BIL3A09",  3, 5, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  60
  ChInfo("BIL_7_3" ,  "BIL3A13",  3, 7, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  61
  ChInfo("BIL_1_-3",  "BIL3C01", -3, 1, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  62
  ChInfo("BIL_2_-3",  "BIL3C03", -3, 2, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  63
  ChInfo("BIL_3_-3",  "BIL3C05", -3, 3, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  64
  ChInfo("BIL_4_-3",  "BIL3C07", -3, 4, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  65
  ChInfo("BIL_5_-3",  "BIL3C09", -3, 5, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  66
  ChInfo("BIL_7_-3",  "BIL3C13", -3, 7, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  67
  ChInfo("BIL_1_4" ,  "BIL4A01",  4, 1, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  68
  ChInfo("BIL_2_4" ,  "BIL4A03",  4, 2, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  69
  ChInfo("BIL_3_4" ,  "BIL4A05",  4, 3, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  70
  ChInfo("BIL_4_4" ,  "BIL4A07",  4, 4, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  71
  ChInfo("BIL_5_4" ,  "BIL4A09",  4, 5, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  72
  ChInfo("BIL_7_4" ,  "BIL4A13",  4, 7, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  73
  ChInfo("BIL_1_-4",  "BIL4C01", -4, 1, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  74
  ChInfo("BIL_2_-4",  "BIL4C03", -4, 2, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  75
  ChInfo("BIL_3_-4",  "BIL4C05", -4, 3, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  76
  ChInfo("BIL_4_-4",  "BIL4C07", -4, 4, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  77
  ChInfo("BIL_5_-4",  "BIL4C09", -4, 5, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  78
  ChInfo("BIL_7_-4",  "BIL4C13", -4, 7, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  79
  ChInfo("BIL_1_5" ,  "BIL5A01",  5, 1, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  80
  ChInfo("BIL_2_5" ,  "BIL5A03",  5, 2, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  81
  ChInfo("BIL_3_5" ,  "BIL5A05",  5, 3, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  82
  ChInfo("BIL_4_5" ,  "BIL5A07",  5, 4, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  83
  ChInfo("BIL_5_5" ,  "BIL5A09",  5, 5, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  84
  ChInfo("BIL_7_5" ,  "BIL5A13",  5, 7, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  85
  ChInfo("BIL_1_-5",  "BIL5C01", -5, 1, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  86
  ChInfo("BIL_2_-5",  "BIL5C03", -5, 2, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  87
  ChInfo("BIL_3_-5",  "BIL5C05", -5, 3, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  88
  ChInfo("BIL_4_-5",  "BIL5C07", -5, 4, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  89
  ChInfo("BIL_5_-5",  "BIL5C09", -5, 5, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  90
  ChInfo("BIL_7_-5",  "BIL5C13", -5, 7, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   #  91
  ChInfo("BIL_1_6" ,  "BIL6A01",  6, 1, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  92
  ChInfo("BIL_2_6" ,  "BIL6A03",  6, 2, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  93
  ChInfo("BIL_3_6" ,  "BIL6A05",  6, 3, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  94
  ChInfo("BIL_4_6" ,  "BIL6A07",  6, 4, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  95
  ChInfo("BIL_5_6" ,  "BIL6A09",  6, 5, "BIL", 2, 4, 36, 4, 36, 12, 1, 4, 3, 2672, 0),   #  96
  ChInfo("BIL_7_6" ,  "BIL6A13",  6, 7, "BIL", 2, 4, 30, 4, 30, 10, 1, 4, 3, 2672, 0),   #  97
  ChInfo("BIL_1_-6",  "BIL6C01", -6, 1, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  98
  ChInfo("BIL_2_-6",  "BIL6C03", -6, 2, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   #  99
  ChInfo("BIL_3_-6",  "BIL6C05", -6, 3, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   # 100
  ChInfo("BIL_4_-6",  "BIL6C07", -6, 4, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   # 101
  ChInfo("BIL_5_-6",  "BIL6C09", -6, 5, "BIL", 2, 4, 36, 4, 36, 12, 2, 3, 4, 2672, 0),   # 102
  ChInfo("BIL_7_-6",  "BIL6C13", -6, 7, "BIL", 2, 4, 30, 4, 30, 10, 2, 3, 4, 2672, 0),   # 103
  ChInfo("BIM_6_1" ,  "BIM1A11",  1, 6, "BIM", 2, 4, 36, 4, 36, 12, 1, 3, 4, 1537, 0),   # 104
  ChInfo("BIM_8_1" ,  "BIM1A15",  1, 8, "BIM", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1537, 0),   # 105
  ChInfo("BIM_6_-1",  "BIM1C11", -1, 6, "BIM", 2, 4, 36, 4, 36, 12, 2, 4, 3, 1537, 0),   # 106
  ChInfo("BIM_8_-1",  "BIM1C15", -1, 8, "BIM", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1537, 0),   # 107
  ChInfo("BIM_6_2" ,  "BIM2A11",  2, 6, "BIM", 2, 4, 36, 4, 36, 12, 1, 3, 4, 1537, 0),   # 108
  ChInfo("BIM_8_2" ,  "BIM2A15",  2, 8, "BIM", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1537, 0),   # 109
  ChInfo("BIM_6_-2",  "BIM2C11", -2, 6, "BIM", 2, 4, 36, 4, 36, 12, 2, 4, 3, 1537, 0),   # 110
  ChInfo("BIM_8_-2",  "BIM2C15", -2, 8, "BIM", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1537, 0),   # 111
  ChInfo("BIM_6_3" ,  "BIM3A11",  3, 6, "BIM", 2, 4, 36, 4, 36, 12, 1, 3, 4, 1537, 0),   # 112
  ChInfo("BIM_8_3" ,  "BIM3A15",  3, 8, "BIM", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1537, 0),   # 113
  ChInfo("BIM_6_-3",  "BIM3C11", -3, 6, "BIM", 2, 4, 36, 4, 36, 12, 2, 4, 3, 1537, 0),   # 114
  ChInfo("BIM_8_-3",  "BIM3C15", -3, 8, "BIM", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1537, 0),   # 115
  ChInfo("BIM_6_4" ,  "BIM4A11",  4, 6, "BIM", 2, 4, 36, 4, 36, 12, 1, 3, 4, 1537, 0),   # 116
  ChInfo("BIM_8_4" ,  "BIM4A15",  4, 8, "BIM", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1537, 0),   # 117
  ChInfo("BIM_6_-4",  "BIM4C11", -4, 6, "BIM", 2, 4, 36, 4, 36, 12, 2, 4, 3, 1537, 0),   # 118
  ChInfo("BIM_8_-4",  "BIM4C15", -4, 8, "BIM", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1537, 0),   # 119
  ChInfo("BIM_6_5" ,  "BIM5A11",  5, 6, "BIM", 2, 4, 36, 4, 36, 12, 1, 3, 4, 1537, 0),   # 120
  ChInfo("BIM_8_5" ,  "BIM5A15",  5, 8, "BIM", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1537, 0),   # 121
  ChInfo("BIM_6_-5",  "BIM5C11", -5, 6, "BIM", 2, 4, 36, 4, 36, 12, 2, 4, 3, 1537, 0),   # 122
  ChInfo("BIM_8_-5",  "BIM5C15", -5, 8, "BIM", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1537, 0),   # 123
  ChInfo("BIR_6_1" ,  "BIR1A11",  1, 6, "BIR", 2, 4, 24, 4, 30,  9, 1, 3, 4, 2672, 0),   # 124
  ChInfo("BIR_8_1" ,  "BIR1A15",  1, 8, "BIR", 2, 4, 24, 4, 30,  9, 1, 4, 3, 2672, 0),   # 125
  ChInfo("BIR_6_-1",  "BIR1C11", -1, 6, "BIR", 2, 4, 24, 4, 30,  9, 2, 4, 3, 2672, 0),   # 126
  ChInfo("BIR_8_-1",  "BIR1C15", -1, 8, "BIR", 2, 4, 24, 4, 30,  9, 2, 3, 4, 2672, 0),   # 127
  ChInfo("BIR_6_2" ,  "BIR2A11",  2, 6, "BIR", 2, 4, 27, 4, 30, 10, 1, 3, 4, 1537, 0),   # 128
  ChInfo("BIR_8_2" ,  "BIR2A15",  2, 8, "BIR", 2, 4, 27, 4, 30, 10, 1, 4, 3, 1537, 0),   # 129
  ChInfo("BIR_6_-2",  "BIR2C11", -2, 6, "BIR", 2, 4, 27, 4, 30, 10, 2, 4, 3, 1537, 0),   # 130
  ChInfo("BIR_8_-2",  "BIR2C15", -2, 8, "BIR", 2, 4, 27, 4, 30, 10, 2, 3, 4, 1537, 0),   # 131
  ChInfo("BIR_6_3" ,  "BIR3A11",  3, 6, "BIR", 2, 4, 33, 4, 33, 12, 1, 3, 4, 1106, 0),   # 132
  ChInfo("BIR_8_3" ,  "BIR3A15",  3, 8, "BIR", 2, 4, 33, 4, 33, 12, 1, 4, 3, 1106, 0),   # 133
  ChInfo("BIR_6_-3",  "BIR3C11", -3, 6, "BIR", 2, 4, 33, 4, 33, 12, 2, 4, 3, 1106, 0),   # 134
  ChInfo("BIR_8_-3",  "BIR3C15", -3, 8, "BIR", 2, 4, 33, 4, 33, 12, 2, 3, 4, 1106, 0),   # 135
  ChInfo("BIR_6_4" ,  "BIR4A11",  4, 6, "BIR", 2, 4, 27, 4, 30, 10, 1, 3, 4, 1537, 0),   # 136
  ChInfo("BIR_8_4" ,  "BIR4A15",  4, 8, "BIR", 2, 4, 27, 4, 30, 10, 1, 4, 3, 1537, 0),   # 137
  ChInfo("BIR_6_-4",  "BIR4C11", -4, 6, "BIR", 2, 4, 27, 4, 30, 10, 2, 4, 3, 1537, 0),   # 138
  ChInfo("BIR_8_-4",  "BIR4C15", -4, 8, "BIR", 2, 4, 27, 4, 30, 10, 2, 3, 4, 1537, 0),   # 139
  ChInfo("BIR_6_5" ,  "BIR5A11",  5, 6, "BIR", 2, 4, 21, 4, 24,  8, 1, 3, 4, 1537, 0),   # 140
  ChInfo("BIR_8_5" ,  "BIR5A15",  5, 8, "BIR", 2, 4, 21, 4, 24,  8, 1, 4, 3, 1537, 0),   # 141
  ChInfo("BIR_6_-5",  "BIR5C11", -5, 6, "BIR", 2, 4, 21, 4, 24,  8, 2, 4, 3, 1537, 0),   # 142
  ChInfo("BIR_8_-5",  "BIR5C15", -5, 8, "BIR", 2, 4, 21, 4, 24,  8, 2, 3, 4, 1537, 0),   # 143
  ChInfo("BIR_6_6" ,  "BIR6A11",  6, 6, "BIR", 2, 4, 36, 4, 36, 12, 1, 3, 4, 1106, 0),   # 144
  ChInfo("BIR_8_6" ,  "BIR6A15",  6, 8, "BIR", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1106, 0),   # 145
  ChInfo("BIR_6_-6",  "BIR6C11", -6, 6, "BIR", 2, 4, 36, 4, 36, 12, 2, 4, 3, 1106, 0),   # 146
  ChInfo("BIR_8_-6",  "BIR6C15", -6, 8, "BIR", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1106, 0),   # 147
  ChInfo("BIS_1_1" ,  "BIS1A02",  1, 1, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 148
  ChInfo("BIS_2_1" ,  "BIS1A04",  1, 2, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 149
  ChInfo("BIS_3_1" ,  "BIS1A06",  1, 3, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 150
  ChInfo("BIS_4_1" ,  "BIS1A08",  1, 4, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 151
  ChInfo("BIS_5_1" ,  "BIS1A10",  1, 5, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 152
  ChInfo("BIS_6_1" ,  "BIS1A12",  1, 6, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 153
  ChInfo("BIS_7_1" ,  "BIS1A14",  1, 7, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 154
  ChInfo("BIS_8_1" ,  "BIS1A16",  1, 8, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 155
  ChInfo("BIS_1_-1",  "BIS1C02", -1, 1, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 156
  ChInfo("BIS_2_-1",  "BIS1C04", -1, 2, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 157
  ChInfo("BIS_3_-1",  "BIS1C06", -1, 3, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 158
  ChInfo("BIS_4_-1",  "BIS1C08", -1, 4, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 159
  ChInfo("BIS_5_-1",  "BIS1C10", -1, 5, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 160
  ChInfo("BIS_6_-1",  "BIS1C12", -1, 6, "BIS", 2, 4, 36, 4, 36, 12, 2, 3, 4, 1672, 0),   # 161
  ChInfo("BIS_7_-1",  "BIS1C14", -1, 7, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 162
  ChInfo("BIS_8_-1",  "BIS1C16", -1, 8, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 163
  ChInfo("BIS_1_2" ,  "BIS2A02",  2, 1, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 164
  ChInfo("BIS_2_2" ,  "BIS2A04",  2, 2, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 165
  ChInfo("BIS_3_2" ,  "BIS2A06",  2, 3, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 166
  ChInfo("BIS_4_2" ,  "BIS2A08",  2, 4, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 167
  ChInfo("BIS_5_2" ,  "BIS2A10",  2, 5, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 168
  ChInfo("BIS_6_2" ,  "BIS2A12",  2, 6, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 169
  ChInfo("BIS_7_2" ,  "BIS2A14",  2, 7, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 170
  ChInfo("BIS_8_2" ,  "BIS2A16",  2, 8, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 171
  ChInfo("BIS_1_-2",  "BIS2C02", -2, 1, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 172
  ChInfo("BIS_2_-2",  "BIS2C04", -2, 2, "BIS", 2, 4, 30, 4, 30, 10, 2, 4, 3, 1672, 0),   # 173
  ChInfo("BIS_3_-2",  "BIS2C06", -2, 3, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 174
  ChInfo("BIS_4_-2",  "BIS2C08", -2, 4, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 175
  ChInfo("BIS_5_-2",  "BIS2C10", -2, 5, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 176
  ChInfo("BIS_6_-2",  "BIS2C12", -2, 6, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 177
  ChInfo("BIS_7_-2",  "BIS2C14", -2, 7, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 178
  ChInfo("BIS_8_-2",  "BIS2C16", -2, 8, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 179
  ChInfo("BIS_1_3" ,  "BIS3A02",  3, 1, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 180
  ChInfo("BIS_2_3" ,  "BIS3A04",  3, 2, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 181
  ChInfo("BIS_3_3" ,  "BIS3A06",  3, 3, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 182
  ChInfo("BIS_4_3" ,  "BIS3A08",  3, 4, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 183
  ChInfo("BIS_5_3" ,  "BIS3A10",  3, 5, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 184
  ChInfo("BIS_6_3" ,  "BIS3A12",  3, 6, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 185
  ChInfo("BIS_7_3" ,  "BIS3A14",  3, 7, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 186
  ChInfo("BIS_8_3" ,  "BIS3A16",  3, 8, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 187
  ChInfo("BIS_1_-3",  "BIS3C02", -3, 1, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 188
  ChInfo("BIS_2_-3",  "BIS3C04", -3, 2, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 189
  ChInfo("BIS_3_-3",  "BIS3C06", -3, 3, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 190
  ChInfo("BIS_4_-3",  "BIS3C08", -3, 4, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 191
  ChInfo("BIS_5_-3",  "BIS3C10", -3, 5, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 192
  ChInfo("BIS_6_-3",  "BIS3C12", -3, 6, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 193
  ChInfo("BIS_7_-3",  "BIS3C14", -3, 7, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 194
  ChInfo("BIS_8_-3",  "BIS3C16", -3, 8, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 195
  ChInfo("BIS_1_4" ,  "BIS4A02",  4, 1, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 196
  ChInfo("BIS_2_4" ,  "BIS4A04",  4, 2, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 197
  ChInfo("BIS_3_4" ,  "BIS4A06",  4, 3, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 198
  ChInfo("BIS_4_4" ,  "BIS4A08",  4, 4, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 199
  ChInfo("BIS_5_4" ,  "BIS4A10",  4, 5, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 200
  ChInfo("BIS_6_4" ,  "BIS4A12",  4, 6, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 201
  ChInfo("BIS_7_4" ,  "BIS4A14",  4, 7, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 202
  ChInfo("BIS_8_4" ,  "BIS4A16",  4, 8, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 203
  ChInfo("BIS_1_-4",  "BIS4C02", -4, 1, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 204
  ChInfo("BIS_2_-4",  "BIS4C04", -4, 2, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 205
  ChInfo("BIS_3_-4",  "BIS4C06", -4, 3, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 206
  ChInfo("BIS_4_-4",  "BIS4C08", -4, 4, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 207
  ChInfo("BIS_5_-4",  "BIS4C10", -4, 5, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 208
  ChInfo("BIS_6_-4",  "BIS4C12", -4, 6, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 209
  ChInfo("BIS_7_-4",  "BIS4C14", -4, 7, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 210
  ChInfo("BIS_8_-4",  "BIS4C16", -4, 8, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 211
  ChInfo("BIS_1_5" ,  "BIS5A02",  5, 1, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 212
  ChInfo("BIS_2_5" ,  "BIS5A04",  5, 2, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 213
  ChInfo("BIS_3_5" ,  "BIS5A06",  5, 3, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 214
  ChInfo("BIS_4_5" ,  "BIS5A08",  5, 4, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 215
  ChInfo("BIS_5_5" ,  "BIS5A10",  5, 5, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 216
  ChInfo("BIS_6_5" ,  "BIS5A12",  5, 6, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 217
  ChInfo("BIS_7_5" ,  "BIS5A14",  5, 7, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 218
  ChInfo("BIS_8_5" ,  "BIS5A16",  5, 8, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 219
  ChInfo("BIS_1_-5",  "BIS5C02", -5, 1, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 220
  ChInfo("BIS_2_-5",  "BIS5C04", -5, 2, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 221
  ChInfo("BIS_3_-5",  "BIS5C06", -5, 3, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 222
  ChInfo("BIS_4_-5",  "BIS5C08", -5, 4, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 223
  ChInfo("BIS_5_-5",  "BIS5C10", -5, 5, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 224
  ChInfo("BIS_6_-5",  "BIS5C12", -5, 6, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 225
  ChInfo("BIS_7_-5",  "BIS5C14", -5, 7, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 226
  ChInfo("BIS_8_-5",  "BIS5C16", -5, 8, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 227
  ChInfo("BIS_1_6" ,  "BIS6A02",  6, 1, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 228
  ChInfo("BIS_2_6" ,  "BIS6A04",  6, 2, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 229
  ChInfo("BIS_3_6" ,  "BIS6A06",  6, 3, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 230
  ChInfo("BIS_4_6" ,  "BIS6A08",  6, 4, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 231
  ChInfo("BIS_5_6" ,  "BIS6A10",  6, 5, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 232
  ChInfo("BIS_6_6" ,  "BIS6A12",  6, 6, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 233
  ChInfo("BIS_7_6" ,  "BIS6A14",  6, 7, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 234
  ChInfo("BIS_8_6" ,  "BIS6A16",  6, 8, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 235
  ChInfo("BIS_1_-6",  "BIS6C02", -6, 1, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 236
  ChInfo("BIS_2_-6",  "BIS6C04", -6, 2, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 237
  ChInfo("BIS_3_-6",  "BIS6C06", -6, 3, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 238
  ChInfo("BIS_4_-6",  "BIS6C08", -6, 4, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 239
  ChInfo("BIS_5_-6",  "BIS6C10", -6, 5, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 240
  ChInfo("BIS_6_-6",  "BIS6C12", -6, 6, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 241
  ChInfo("BIS_7_-6",  "BIS6C14", -6, 7, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 242
  ChInfo("BIS_8_-6",  "BIS6C16", -6, 8, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 243
  ChInfo("BIS_1_7" ,  "BIS7A02",  7, 1, "BIS", 2, 4, 78, 4,108, 31, 1, 5, 5, 1672, 1),   # 244 sMDT
  ChInfo("BIS_2_7" ,  "BIS7A04",  7, 2, "BIS", 2, 4, 78, 4,108, 31, 1, 5, 5, 1672, 1),   # 245 sMDT
  ChInfo("BIS_3_7" ,  "BIS7A06",  7, 3, "BIS", 2, 4, 78, 4,108, 31, 1, 5, 5, 1672, 1),   # 246 sMDT
  ChInfo("BIS_4_7" ,  "BIS7A08",  7, 4, "BIS", 2, 4, 65, 4, 96, 27, 1, 5, 5, 1672, 1),   # 247 sMDT
  ChInfo("BIS_5_7" ,  "BIS7A10",  7, 5, "BIS", 2, 4, 65, 4, 96, 27, 1, 5, 5, 1672, 1),   # 248 sMDT
  ChInfo("BIS_6_7" ,  "BIS7A12",  7, 6, "BIS", 2, 4, 66, 4, 96, 27, 1, 5, 5, 1672, 1),   # 249 sMDT
  ChInfo("BIS_7_7" ,  "BIS7A14",  7, 7, "BIS", 2, 4, 66, 4, 96, 27, 1, 5, 5, 1672, 1),   # 250 sMDT
  ChInfo("BIS_8_7" ,  "BIS7A16",  7, 8, "BIS", 2, 4, 66, 4, 96, 27, 1, 5, 5, 1672, 1),   # 251 sMDT
  ChInfo("BIS_1_-7",  "BIS7C02", -7, 1, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 252
  ChInfo("BIS_2_-7",  "BIS7C04", -7, 2, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 253
  ChInfo("BIS_3_-7",  "BIS7C06", -7, 3, "BIS", 2, 4, 36, 4, 36, 12, 1, 4, 3, 1672, 0),   # 254
  ChInfo("BIS_4_-7",  "BIS7C08", -7, 4, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 255
  ChInfo("BIS_5_-7",  "BIS7C10", -7, 5, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 256
  ChInfo("BIS_6_-7",  "BIS7C12", -7, 6, "BIS", 2, 4, 30, 4, 30, 10, 2, 3, 4, 1672, 0),   # 257
  ChInfo("BIS_7_-7",  "BIS7C14", -7, 7, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 258
  ChInfo("BIS_8_-7",  "BIS7C16", -7, 8, "BIS", 2, 4, 30, 4, 30, 10, 1, 4, 3, 1672, 0),   # 259
  ChInfo("BIS_1_-8",  "BIS8C02", -8, 1, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 260
  ChInfo("BIS_2_-8",  "BIS8C04", -8, 2, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 261
  ChInfo("BIS_3_-8",  "BIS8C06", -8, 3, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 262
  ChInfo("BIS_4_-8",  "BIS8C08", -8, 4, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 263
  ChInfo("BIS_5_-8",  "BIS8C10", -8, 5, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 364
  ChInfo("BIS_6_-8",  "BIS8C12", -8, 6, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 265
  ChInfo("BIS_7_-8",  "BIS8C14", -8, 7, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 366
  ChInfo("BIS_8_-8",  "BIS8C16", -8, 8, "BIS", 1, 3, 16, 0,  0,  2, 1, 1, 0,  852, 0),   # 367
  ChInfo("BME_7_1" ,  "BME4A13",  1, 7, "BME", 2, 4, 78, 4, 78, 26, 1, 6, 6, 2159, 1),   # 268 sMDT
  ChInfo("BME_7_-1",  "BME4C13", -1, 7, "BME", 2, 4, 78, 4, 78, 26, 1, 5, 5, 2159, 1),   # 269 sMDT
  ChInfo("BMF_6_1" ,  "BMF1A12",  1, 6, "BMF", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3072, 0),   # 270 
  ChInfo("BMF_7_1" ,  "BMF1A14",  1, 7, "BMF", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3072, 0),   # 271 
  ChInfo("BMF_6_-1",  "BMF1C12", -1, 6, "BMF", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3072, 0),   # 272 
  ChInfo("BMF_7_-1",  "BMF1C14", -1, 7, "BMF", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3072, 0),   # 273 
  ChInfo("BMF_6_2" ,  "BMF2A12",  2, 6, "BMF", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3072, 0),   # 274 
  ChInfo("BMF_7_2" ,  "BMF2A14",  2, 7, "BMF", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3072, 0),   # 275 
  ChInfo("BMF_6_-2",  "BMF2C12", -2, 6, "BMF", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3072, 0),   # 276 
  ChInfo("BMF_7_-2",  "BMF2C14", -2, 7, "BMF", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3072, 0),   # 277 
  ChInfo("BMF_6_3" ,  "BMF3A12",  3, 6, "BMF", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 278 
  ChInfo("BMF_7_3" ,  "BMF3A14",  3, 7, "BMF", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 279 
  ChInfo("BMF_6_-3",  "BMF3C12", -3, 6, "BMF", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 280 
  ChInfo("BMF_7_-3",  "BMF3C14", -3, 7, "BMF", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 281 
  ChInfo("BMG_6_1" ,  "BMG2A12",  1, 6, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 282 sMDT
  ChInfo("BMG_7_1" ,  "BMG2A14",  1, 7, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 283 sMDT
  ChInfo("BMG_6_-1",  "BMG2C12", -1, 6, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 284 sMDT
  ChInfo("BMG_7_-1",  "BMG2C14", -1, 7, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 285 sMDT
  ChInfo("BMG_6_2" ,  "BMG4A12",  2, 6, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 286 sMDT 
  ChInfo("BMG_7_2" ,  "BMG4A14",  2, 7, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 287 sMDT
  ChInfo("BMG_6_-2",  "BMG4C12", -2, 6, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 288 sMDT
  ChInfo("BMG_7_-2",  "BMG4C14", -2, 7, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 289 sMDT
  ChInfo("BMG_6_3" ,  "BMG6A12",  3, 6, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 290 sMDT
  ChInfo("BMG_7_3" ,  "BMG6A14",  3, 7, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 291 sMDT
  ChInfo("BMG_6_-3",  "BMG6C12", -3, 6, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 292 sMDT
  ChInfo("BMG_7_-3",  "BMG6C14", -3, 7, "BMG", 2, 4, 54, 4, 54, 18, 2, 6, 6, 1129, 1),   # 293 sMDT
  ChInfo("BML_1_1" ,  "BML1A01",  1, 1, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 294 
  ChInfo("BML_2_1" ,  "BML1A03",  1, 2, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 295 
  ChInfo("BML_3_1" ,  "BML1A05",  1, 3, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 296 
  ChInfo("BML_4_1" ,  "BML1A07",  1, 4, "BML", 2, 3, 32, 3, 32, 8 , 1, 2, 1, 3552, 0),   # 297 
  ChInfo("BML_5_1" ,  "BML1A09",  1, 5, "BML", 2, 3, 32, 3, 32, 8 , 1, 2, 1, 3552, 0),   # 298 
  ChInfo("BML_6_1" ,  "BML1A11",  1, 6, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 299 
  ChInfo("BML_7_1" ,  "BML1A13",  1, 7, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 300 
  ChInfo("BML_8_1" ,  "BML1A15",  1, 8, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 301 
  ChInfo("BML_1_-1",  "BML1C01", -1, 1, "BML", 2, 3, 32, 3, 32, 8 , 2, 1, 2, 3552, 0),   # 302 
  ChInfo("BML_2_-1",  "BML1C03", -1, 2, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 303 
  ChInfo("BML_3_-1",  "BML1C05", -1, 3, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 304 
  ChInfo("BML_4_-1",  "BML1C07", -1, 4, "BML", 2, 3, 32, 3, 32, 8 , 2, 1, 2, 3552, 0),   # 305 
  ChInfo("BML_5_-1",  "BML1C09", -1, 5, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 306 
  ChInfo("BML_6_-1",  "BML1C11", -1, 6, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 307 
  ChInfo("BML_7_-1",  "BML1C13", -1, 7, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 308 
  ChInfo("BML_8_-1",  "BML1C15", -1, 8, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 309 
  ChInfo("BML_1_2" ,  "BML2A01",  2, 1, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 310 
  ChInfo("BML_2_2" ,  "BML2A03",  2, 2, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 311 
  ChInfo("BML_3_2" ,  "BML2A05",  2, 3, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 312 
  ChInfo("BML_4_2" ,  "BML2A07",  2, 4, "BML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3552, 0),   # 313 
  ChInfo("BML_5_2" ,  "BML2A09",  2, 5, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 314 
  ChInfo("BML_6_2" ,  "BML2A11",  2, 6, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 315 
  ChInfo("BML_7_2" ,  "BML2A13",  2, 7, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 316 
  ChInfo("BML_8_2" ,  "BML2A15",  2, 8, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 317 
  ChInfo("BML_1_-2",  "BML2C01", -2, 1, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 318 
  ChInfo("BML_2_-2",  "BML2C03", -2, 2, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 319 
  ChInfo("BML_3_-2",  "BML2C05", -2, 3, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 320 
  ChInfo("BML_4_-2",  "BML2C07", -2, 4, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 321 
  ChInfo("BML_5_-2",  "BML2C09", -2, 5, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 322 
  ChInfo("BML_6_-2",  "BML2C11", -2, 6, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 323 
  ChInfo("BML_7_-2",  "BML2C13", -2, 7, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 324 
  ChInfo("BML_8_-2",  "BML2C15", -2, 8, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 325 
  ChInfo("BML_1_3" ,  "BML3A01",  3, 1, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 326 
  ChInfo("BML_2_3" ,  "BML3A03",  3, 2, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 327 
  ChInfo("BML_3_3" ,  "BML3A05",  3, 3, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 328 
  ChInfo("BML_4_3" ,  "BML3A07",  3, 4, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 329 
  ChInfo("BML_5_3" ,  "BML3A09",  3, 5, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 330 
  ChInfo("BML_6_3" ,  "BML3A11",  3, 6, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 331 
  ChInfo("BML_7_3" ,  "BML3A13",  3, 7, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 332 
  ChInfo("BML_8_3" ,  "BML3A15",  3, 8, "BML", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3552, 0),   # 333 
  ChInfo("BML_1_-3",  "BML3C01", -3, 1, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 334 
  ChInfo("BML_2_-3",  "BML3C03", -3, 2, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 335 
  ChInfo("BML_3_-3",  "BML3C05", -3, 3, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 336 
  ChInfo("BML_4_-3",  "BML3C07", -3, 4, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 337 
  ChInfo("BML_5_-3",  "BML3C09", -3, 5, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 338 
  ChInfo("BML_6_-3",  "BML3C11", -3, 6, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 339 
  ChInfo("BML_7_-3",  "BML3C13", -3, 7, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 340 
  ChInfo("BML_8_-3",  "BML3C15", -3, 8, "BML", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3552, 0),   # 341 
  ChInfo("BML_1_4" ,  "BML4A01",  4, 1, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 342 
  ChInfo("BML_2_4" ,  "BML4A03",  4, 2, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 343 
  ChInfo("BML_3_4" ,  "BML4A05",  4, 3, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 344 
  ChInfo("BML_4_4" ,  "BML4A07",  4, 4, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 345 
  ChInfo("BML_5_4" ,  "BML4A09",  4, 5, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 346 
  ChInfo("BML_6_4" ,  "BML4A11",  4, 6, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 347 
  ChInfo("BML_8_4" ,  "BML4A15",  4, 8, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 348 
  ChInfo("BML_1_-4",  "BML4C01", -4, 1, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 349 
  ChInfo("BML_2_-4",  "BML4C03", -4, 2, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 350 
  ChInfo("BML_3_-4",  "BML4C05", -4, 3, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 351 
  ChInfo("BML_4_-4",  "BML4C07", -4, 4, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 352 
  ChInfo("BML_5_-4",  "BML4C09", -4, 5, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 353 
  ChInfo("BML_6_-4",  "BML4C11", -4, 6, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 354 
  ChInfo("BML_8_-4",  "BML4C15", -4, 8, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 355 
  ChInfo("BML_1_5" ,  "BML5A01",  5, 1, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 356 
  ChInfo("BML_2_5" ,  "BML5A03",  5, 2, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 357 
  ChInfo("BML_3_5" ,  "BML5A05",  5, 3, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 358 
  ChInfo("BML_4_5" ,  "BML5A07",  5, 4, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 359 
  ChInfo("BML_5_5" ,  "BML5A09",  5, 5, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 360 
  ChInfo("BML_6_5" ,  "BML5A11",  5, 6, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 361 
  ChInfo("BML_7_4" ,  "BML5A13",  4, 7, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 362 
  ChInfo("BML_8_5" ,  "BML5A15",  5, 8, "BML", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3552, 0),   # 363 
  ChInfo("BML_1_-5",  "BML5C01", -5, 1, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 364 
  ChInfo("BML_2_-5",  "BML5C03", -5, 2, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 365 
  ChInfo("BML_3_-5",  "BML5C05", -5, 3, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 366 
  ChInfo("BML_4_-5",  "BML5C07", -5, 4, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 367 
  ChInfo("BML_5_-5",  "BML5C09", -5, 5, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 368 
  ChInfo("BML_6_-5",  "BML5C11", -5, 6, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 369 
  ChInfo("BML_7_-4",  "BML5C13", -4, 7, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 370 
  ChInfo("BML_8_-5",  "BML5C15", -5, 8, "BML", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3552, 0),   # 371 
  ChInfo("BML_1_6" ,  "BML6A01",  6, 1, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 372 
  ChInfo("BML_2_6" ,  "BML6A03",  6, 2, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 373 
  ChInfo("BML_3_6" ,  "BML6A05",  6, 3, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 374 
  ChInfo("BML_4_6" ,  "BML6A07",  6, 4, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 375 
  ChInfo("BML_5_6" ,  "BML6A09",  6, 5, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 376 
  ChInfo("BML_6_6" ,  "BML6A11",  6, 6, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 377 
  ChInfo("BML_7_5" ,  "BML6A13",  5, 7, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 378 
  ChInfo("BML_8_6" ,  "BML6A15",  6, 8, "BML", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3552, 0),   # 379 
  ChInfo("BML_1_-6",  "BML6C01", -6, 1, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 380 
  ChInfo("BML_2_-6",  "BML6C03", -6, 2, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 381 
  ChInfo("BML_3_-6",  "BML6C05", -6, 3, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 382 
  ChInfo("BML_4_-6",  "BML6C07", -6, 4, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 383 
  ChInfo("BML_5_-6",  "BML6C09", -6, 5, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 384 
  ChInfo("BML_6_-6",  "BML6C11", -6, 6, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 385 
  ChInfo("BML_7_-5",  "BML6C13", -5, 7, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 386 
  ChInfo("BML_8_-6",  "BML6C15", -6, 8, "BML", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3552, 0),   # 387 
  ChInfo("BMS_1_1" ,  "BMS1A02",  1, 1, "BMS", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3072, 0),   # 388 
  ChInfo("BMS_2_1" ,  "BMS1A04",  1, 2, "BMS", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3072, 0),   # 389 
  ChInfo("BMS_3_1" ,  "BMS1A06",  1, 3, "BMS", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3072, 0),   # 390 
  ChInfo("BMS_4_1" ,  "BMS1A08",  1, 4, "BMS", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3072, 0),   # 391 
  ChInfo("BMS_5_1" ,  "BMS1A10",  1, 5, "BMS", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3072, 0),   # 392 
  ChInfo("BMS_8_1" ,  "BMS1A16",  1, 8, "BMS", 2, 3, 56, 3, 56, 14, 2, 1, 2, 3072, 0),   # 393 
  ChInfo("BMS_1_-1",  "BMS1C02", -1, 1, "BMS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3072, 0),   # 394 
  ChInfo("BMS_2_-1",  "BMS1C04", -1, 2, "BMS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3072, 0),   # 395 
  ChInfo("BMS_3_-1",  "BMS1C06", -1, 3, "BMS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3072, 0),   # 396 
  ChInfo("BMS_4_-1",  "BMS1C08", -1, 4, "BMS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3072, 0),   # 397 
  ChInfo("BMS_5_-1",  "BMS1C10", -1, 5, "BMS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3072, 0),   # 398 
  ChInfo("BMS_8_-1",  "BMS1C16", -1, 8, "BMS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 3072, 0),   # 399 
  ChInfo("BMS_1_2" ,  "BMS2A02",  2, 1, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 400 
  ChInfo("BMS_2_2" ,  "BMS2A04",  2, 2, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 401 
  ChInfo("BMS_3_2" ,  "BMS2A06",  2, 3, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 402 
  ChInfo("BMS_4_2" ,  "BMS2A08",  2, 4, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 403 
  ChInfo("BMS_5_2" ,  "BMS2A10",  2, 5, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 404 
  ChInfo("BMS_8_2" ,  "BMS2A16",  2, 8, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 405 
  ChInfo("BMS_1_-2",  "BMS2C02", -2, 1, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 406 
  ChInfo("BMS_2_-2",  "BMS2C04", -2, 2, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 407 
  ChInfo("BMS_3_-2",  "BMS2C06", -2, 3, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 408 
  ChInfo("BMS_4_-2",  "BMS2C08", -2, 4, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 409 
  ChInfo("BMS_5_-2",  "BMS2C10", -2, 5, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 410 
  ChInfo("BMS_8_-2",  "BMS2C16", -2, 8, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 411 
  ChInfo("BMS_1_3" ,  "BMS3A02",  3, 1, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 412 
  ChInfo("BMS_2_3" ,  "BMS3A04",  3, 2, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 413 
  ChInfo("BMS_3_3" ,  "BMS3A06",  3, 3, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 414 
  ChInfo("BMS_4_3" ,  "BMS3A08",  3, 4, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 415 
  ChInfo("BMS_5_3" ,  "BMS3A10",  3, 5, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 416 
  ChInfo("BMS_8_3" ,  "BMS3A16",  3, 8, "BMS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3072, 0),   # 417 
  ChInfo("BMS_1_-3",  "BMS3C02", -3, 1, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 418 
  ChInfo("BMS_2_-3",  "BMS3C04", -3, 2, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 419 
  ChInfo("BMS_3_-3",  "BMS3C06", -3, 3, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 420 
  ChInfo("BMS_4_-3",  "BMS3C08", -3, 4, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 421 
  ChInfo("BMS_5_-3",  "BMS3C10", -3, 5, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 422 
  ChInfo("BMS_8_-3",  "BMS3C16", -3, 8, "BMS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3072, 0),   # 423 
  ChInfo("BMS_1_4" ,  "BMS4A02",  4, 1, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 424 
  ChInfo("BMS_2_4" ,  "BMS4A04",  4, 2, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 425 
  ChInfo("BMS_3_4" ,  "BMS4A06",  4, 3, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 426 
  ChInfo("BMS_4_4" ,  "BMS4A08",  4, 4, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 427 
  ChInfo("BMS_5_4" ,  "BMS4A10",  4, 5, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 428 
  ChInfo("BMS_8_4" ,  "BMS4A16",  4, 8, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 429 
  ChInfo("BMS_1_-4",  "BMS4C02", -4, 1, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 430 
  ChInfo("BMS_2_-4",  "BMS4C04", -4, 2, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 431 
  ChInfo("BMS_3_-4",  "BMS4C06", -4, 3, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 432 
  ChInfo("BMS_4_-4",  "BMS4C08", -4, 4, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 433 
  ChInfo("BMS_5_-4",  "BMS4C10", -4, 5, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 434 
  ChInfo("BMS_8_-4",  "BMS4C16", -4, 8, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 435 
  ChInfo("BMS_1_5" ,  "BMS5A02",  5, 1, "BMS", 2, 3, 32, 3, 32,  8, 2, 1, 2, 3072, 0),   # 436 
  ChInfo("BMS_2_5" ,  "BMS5A04",  5, 2, "BMS", 2, 3, 32, 3, 32,  8, 2, 1, 2, 3072, 0),   # 437 
  ChInfo("BMS_3_5" ,  "BMS5A06",  5, 3, "BMS", 2, 3, 32, 3, 32,  8, 2, 1, 2, 3072, 0),   # 438 
  ChInfo("BMS_4_5" ,  "BMS5A08",  5, 4, "BMS", 2, 3, 32, 3, 32,  8, 2, 1, 2, 3072, 0),   # 439 
  ChInfo("BMS_5_5" ,  "BMS5A10",  5, 5, "BMS", 2, 3, 32, 3, 32,  8, 2, 1, 2, 3072, 0),   # 440 
  ChInfo("BMS_8_5" ,  "BMS5A16",  5, 8, "BMS", 2, 3, 32, 3, 32,  8, 2, 1, 2, 3072, 0),   # 441 
  ChInfo("BMS_1_-5",  "BMS5C02", -5, 1, "BMS", 2, 3, 32, 3, 32,  8, 1, 2, 1, 3072, 0),   # 442 
  ChInfo("BMS_2_-5",  "BMS5C04", -5, 2, "BMS", 2, 3, 32, 3, 32,  8, 1, 2, 1, 3072, 0),   # 443 
  ChInfo("BMS_3_-5",  "BMS5C06", -5, 3, "BMS", 2, 3, 32, 3, 32,  8, 1, 2, 1, 3072, 0),   # 444 
  ChInfo("BMS_4_-5",  "BMS5C08", -5, 4, "BMS", 2, 3, 32, 3, 32,  8, 1, 2, 1, 3072, 0),   # 445 
  ChInfo("BMS_5_-5",  "BMS5C10", -5, 5, "BMS", 2, 3, 32, 3, 32,  8, 1, 2, 1, 3072, 0),   # 446 
  ChInfo("BMS_8_-5",  "BMS5C16", -5, 8, "BMS", 2, 3, 32, 3, 32,  8, 1, 2, 1, 3072, 0),   # 447 
  ChInfo("BMS_1_6" ,  "BMS6A02",  6, 1, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 448 
  ChInfo("BMS_2_6" ,  "BMS6A04",  6, 2, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 449 
  ChInfo("BMS_3_6" ,  "BMS6A06",  6, 3, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 450 
  ChInfo("BMS_4_6" ,  "BMS6A08",  6, 4, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 451 
  ChInfo("BMS_5_6" ,  "BMS6A10",  6, 5, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 452 
  ChInfo("BMS_8_6" ,  "BMS6A16",  6, 8, "BMS", 2, 3, 40, 3, 48, 11, 2, 1, 2, 3072, 0),   # 453 
  ChInfo("BMS_1_-6",  "BMS6C02", -6, 1, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 454 
  ChInfo("BMS_2_-6",  "BMS6C04", -6, 2, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 455 
  ChInfo("BMS_3_-6",  "BMS6C06", -6, 3, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 456 
  ChInfo("BMS_4_-6",  "BMS6C08", -6, 4, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 457 
  ChInfo("BMS_5_-6",  "BMS6C10", -6, 5, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 458 
  ChInfo("BMS_8_-6",  "BMS6C16", -6, 8, "BMS", 2, 3, 40, 3, 48, 11, 1, 2, 1, 3072, 0),   # 459 
  ChInfo("BOF_6_1" ,  "BOF1A12",  1, 6, "BOF", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 460 
  ChInfo("BOF_7_1" ,  "BOF1A14",  1, 7, "BOF", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 461 
  ChInfo("BOF_6_-1",  "BOF1C12", -1, 6, "BOF", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 462 
  ChInfo("BOF_7_-1",  "BOF1C14", -1, 7, "BOF", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 463 
  ChInfo("BOF_6_2" ,  "BOF3A12",  2, 6, "BOF", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 464 
  ChInfo("BOF_7_2" ,  "BOF3A14",  2, 7, "BOF", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 465 
  ChInfo("BOF_6_-2",  "BOF3C12", -2, 6, "BOF", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 466 
  ChInfo("BOF_7_-2",  "BOF3C14", -2, 7, "BOF", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 467 
  ChInfo("BOF_6_3" ,  "BOF5A12",  3, 6, "BOF", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3773, 0),   # 468 
  ChInfo("BOF_7_3" ,  "BOF5A14",  3, 7, "BOF", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3773, 0),   # 469 
  ChInfo("BOF_6_-3",  "BOF5C12", -3, 6, "BOF", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3773, 0),   # 470 
  ChInfo("BOF_7_-3",  "BOF5C14", -3, 7, "BOF", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3773, 0),   # 471 
  ChInfo("BOF_6_4" ,  "BOF7A12",  4, 6, "BOF", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3773, 0),   # 472 
  ChInfo("BOF_7_4" ,  "BOF7A14",  4, 7, "BOF", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3773, 0),   # 473 
  ChInfo("BOF_6_-4",  "BOF7C12", -4, 6, "BOF", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3773, 0),   # 474 
  ChInfo("BOF_7_-4",  "BOF7C14", -4, 7, "BOF", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3773, 0),   # 475 
  ChInfo("BOG_6_0" ,  "BOG0A12",  0, 6, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 476 
  ChInfo("BOG_7_0" ,  "BOG0A14",  0, 7, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 477 
  ChInfo("BOG_6_1" ,  "BOG2A12",  1, 6, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 478 
  ChInfo("BOG_7_1" ,  "BOG2A14",  1, 7, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 479 
  ChInfo("BOG_6_-1",  "BOG2C12", -1, 6, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 480 
  ChInfo("BOG_7_-1",  "BOG2C14", -1, 7, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 481 
  ChInfo("BOG_6_2" ,  "BOG4A12",  2, 6, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 482 
  ChInfo("BOG_7_2" ,  "BOG4A14",  2, 7, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 483 
  ChInfo("BOG_6_-2",  "BOG4C12", -2, 6, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 484 
  ChInfo("BOG_7_-2",  "BOG4C14", -2, 7, "BOG", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3772, 0),   # 485 
  ChInfo("BOG_6_3" ,  "BOG6A12",  3, 6, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 486 
  ChInfo("BOG_7_3" ,  "BOG6A14",  3, 7, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 487 
  ChInfo("BOG_6_-3",  "BOG6C12", -3, 6, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 488 
  ChInfo("BOG_7_-3",  "BOG6C14", -3, 7, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 489 
  ChInfo("BOG_6_4" ,  "BOG8A12",  4, 6, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 490 
  ChInfo("BOG_7_4" ,  "BOG8A14",  4, 7, "BOG", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3772, 0),   # 491 
  ChInfo("BOG_6_-4",  "BOG8C12", -4, 6, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 492 
  ChInfo("BOG_7_-4",  "BOG8C14", -4, 7, "BOG", 2, 3, 40, 3, 40, 10, 1, 2, 1, 3772, 0),   # 493 
  ChInfo("BOL_1_1" ,  "BOL1A01",  1, 1, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 494 
  ChInfo("BOL_2_1" ,  "BOL1A03",  1, 2, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 495 
  ChInfo("BOL_3_1" ,  "BOL1A05",  1, 3, "BOL", 2, 3, 64, 3, 64, 16, 1, 2, 1, 4962, 0),   # 496 
  ChInfo("BOL_4_1" ,  "BOL1A07",  1, 4, "BOL", 2, 3, 48, 3, 48, 12, 1, 2, 1, 4962, 0),   # 497 
  ChInfo("BOL_5_1" ,  "BOL1A09",  1, 5, "BOL", 2, 3, 48, 3, 48, 12, 1, 2, 1, 4962, 0),   # 498 
  ChInfo("BOL_6_1" ,  "BOL1A11",  1, 6, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 499 
  ChInfo("BOL_7_1" ,  "BOL1A13",  1, 7, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 500 
  ChInfo("BOL_8_1" ,  "BOL1A15",  1, 8, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 501 
  ChInfo("BOL_1_-1",  "BOL1C01", -1, 1, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 502 
  ChInfo("BOL_2_-1",  "BOL1C03", -1, 2, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 503 
  ChInfo("BOL_3_-1",  "BOL1C05", -1, 3, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 504 
  ChInfo("BOL_4_-1",  "BOL1C07", -1, 4, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 505 
  ChInfo("BOL_5_-1",  "BOL1C09", -1, 5, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 506 
  ChInfo("BOL_6_-1",  "BOL1C11", -1, 6, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 507 
  ChInfo("BOL_7_-1",  "BOL1C13", -1, 7, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 508 
  ChInfo("BOL_8_-1",  "BOL1C15", -1, 8, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 509 
  ChInfo("BOL_1_2" ,  "BOL2A01",  2, 1, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 510 
  ChInfo("BOL_2_2" ,  "BOL2A03",  2, 2, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 511 
  ChInfo("BOL_3_2" ,  "BOL2A05",  2, 3, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 512 
  ChInfo("BOL_4_2" ,  "BOL2A07",  2, 4, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 513 
  ChInfo("BOL_5_2" ,  "BOL2A09",  2, 5, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 514 
  ChInfo("BOL_6_2" ,  "BOL2A11",  2, 6, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 515 
  ChInfo("BOL_7_2" ,  "BOL2A13",  2, 7, "BOL", 2, 3, 48, 3, 48, 12, 1, 2, 1, 4962, 0),   # 516 
  ChInfo("BOL_8_2" ,  "BOL2A15",  2, 8, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 517 
  ChInfo("BOL_1_-2",  "BOL2C01", -2, 1, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 518 
  ChInfo("BOL_2_-2",  "BOL2C03", -2, 2, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 519 
  ChInfo("BOL_3_-2",  "BOL2C05", -2, 3, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 520 
  ChInfo("BOL_4_-2",  "BOL2C07", -2, 4, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 521 
  ChInfo("BOL_5_-2",  "BOL2C09", -2, 5, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 522 
  ChInfo("BOL_6_-2",  "BOL2C11", -2, 6, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 523 
  ChInfo("BOL_7_-2",  "BOL2C13", -2, 7, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 524 
  ChInfo("BOL_8_-2",  "BOL2C15", -2, 8, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 525 
  ChInfo("BOL_1_3" ,  "BOL3A01",  3, 1, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 526 
  ChInfo("BOL_2_3" ,  "BOL3A03",  3, 2, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 527 
  ChInfo("BOL_3_3" ,  "BOL3A05",  3, 3, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 528 
  ChInfo("BOL_4_3" ,  "BOL3A07",  3, 4, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 529 
  ChInfo("BOL_5_3" ,  "BOL3A09",  3, 5, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 530 
  ChInfo("BOL_6_3" ,  "BOL3A11",  3, 6, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 531 
  ChInfo("BOL_7_3" ,  "BOL3A13",  3, 7, "BOL", 2, 3, 48, 3, 48, 12, 1, 2, 1, 4962, 0),   # 532 
  ChInfo("BOL_8_3" ,  "BOL3A15",  3, 8, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 533 
  ChInfo("BOL_1_-3",  "BOL3C01", -3, 1, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 534 
  ChInfo("BOL_2_-3",  "BOL3C03", -3, 2, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 535 
  ChInfo("BOL_3_-3",  "BOL3C05", -3, 3, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 536 
  ChInfo("BOL_4_-3",  "BOL3C07", -3, 4, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 537 
  ChInfo("BOL_5_-3",  "BOL3C09", -3, 5, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 538 
  ChInfo("BOL_6_-3",  "BOL3C11", -3, 6, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 539 
  ChInfo("BOL_7_-3",  "BOL3C13", -3, 7, "BOL", 2, 3, 48, 3, 48, 12, 2, 1, 2, 4962, 0),   # 540 
  ChInfo("BOL_8_-3",  "BOL3C15", -3, 8, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 541 
  ChInfo("BOL_1_4" ,  "BOL4A01",  4, 1, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 542 
  ChInfo("BOL_2_4" ,  "BOL4A03",  4, 2, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 543 
  ChInfo("BOL_3_4" ,  "BOL4A05",  4, 3, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 544 
  ChInfo("BOL_4_4" ,  "BOL4A07",  4, 4, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 545 
  ChInfo("BOL_5_4" ,  "BOL4A09",  4, 5, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 546 
  ChInfo("BOL_6_4" ,  "BOL4A11",  4, 6, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 547 
  ChInfo("BOL_7_4" ,  "BOL4A13",  4, 7, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 548 
  ChInfo("BOL_8_4" ,  "BOL4A15",  4, 8, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 549 
  ChInfo("BOL_1_-4",  "BOL4C01", -4, 1, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 550 
  ChInfo("BOL_2_-4",  "BOL4C03", -4, 2, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 551 
  ChInfo("BOL_3_-4",  "BOL4C05", -4, 3, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 552 
  ChInfo("BOL_4_-4",  "BOL4C07", -4, 4, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 553 
  ChInfo("BOL_5_-4",  "BOL4C09", -4, 5, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 554 
  ChInfo("BOL_6_-4",  "BOL4C11", -4, 6, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 555 
  ChInfo("BOL_7_-4",  "BOL4C13", -4, 7, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 556 
  ChInfo("BOL_8_-4",  "BOL4C15", -4, 8, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 557 
  ChInfo("BOL_1_5" ,  "BOL5A01",  5, 1, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 558 
  ChInfo("BOL_2_5" ,  "BOL5A03",  5, 2, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 559 
  ChInfo("BOL_3_5" ,  "BOL5A05",  5, 3, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 560 
  ChInfo("BOL_4_5" ,  "BOL5A07",  5, 4, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 561 
  ChInfo("BOL_5_5" ,  "BOL5A09",  5, 5, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 562 
  ChInfo("BOL_6_5" ,  "BOL5A11",  5, 6, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 563 
  ChInfo("BOL_7_5" ,  "BOL5A13",  5, 7, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 564 
  ChInfo("BOL_8_5" ,  "BOL5A15",  5, 8, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 565 
  ChInfo("BOL_1_-5",  "BOL5C01", -5, 1, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 566 
  ChInfo("BOL_2_-5",  "BOL5C03", -5, 2, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 567 
  ChInfo("BOL_3_-5",  "BOL5C05", -5, 3, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 568 
  ChInfo("BOL_4_-5",  "BOL5C07", -5, 4, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 569 
  ChInfo("BOL_5_-5",  "BOL5C09", -5, 5, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 570 
  ChInfo("BOL_6_-5",  "BOL5C11", -5, 6, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 571 
  ChInfo("BOL_7_-5",  "BOL5C13", -5, 7, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 572 
  ChInfo("BOL_8_-5",  "BOL5C15", -5, 8, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 573 
  ChInfo("BOL_1_6" ,  "BOL6A01",  6, 1, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 574 
  ChInfo("BOL_2_6" ,  "BOL6A03",  6, 2, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 575 
  ChInfo("BOL_3_6" ,  "BOL6A05",  6, 3, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 576 
  ChInfo("BOL_4_6" ,  "BOL6A07",  6, 4, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 577 
  ChInfo("BOL_5_6" ,  "BOL6A09",  6, 5, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 578 
  ChInfo("BOL_6_6" ,  "BOL6A11",  6, 6, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 579 
  ChInfo("BOL_7_6" ,  "BOL6A13",  6, 7, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 580 
  ChInfo("BOL_8_6" ,  "BOL6A15",  6, 8, "BOL", 2, 3, 56, 3, 56, 14, 1, 2, 1, 4962, 0),   # 581 
  ChInfo("BOL_7_7" ,  "BOE3A13",  7, 7, "BOL", 2, 3, 72, 3, 72, 18, 2, 1, 2, 4962, 0),   # 582 
  ChInfo("BOL_1_-6",  "BOL6C01", -6, 1, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 583 
  ChInfo("BOL_2_-6",  "BOL6C03", -6, 2, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 584 
  ChInfo("BOL_3_-6",  "BOL6C05", -6, 3, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 585 
  ChInfo("BOL_4_-6",  "BOL6C07", -6, 4, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 586 
  ChInfo("BOL_5_-6",  "BOL6C09", -6, 5, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 587 
  ChInfo("BOL_6_-6",  "BOL6C11", -6, 6, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 588 
  ChInfo("BOL_7_-6",  "BOL6C13", -6, 7, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 589 
  ChInfo("BOL_8_-6",  "BOL6C15", -6, 8, "BOL", 2, 3, 56, 3, 56, 14, 2, 1, 2, 4962, 0),   # 590 
  ChInfo("BOL_7_-7",  "BOE3C13", -7, 7, "BOL", 2, 3, 72, 3, 72, 18, 1, 2, 1, 4962, 0),   # 591 
  ChInfo("BOS_1_1" ,  "BOS1A02",  1, 1, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 592 
  ChInfo("BOS_2_1" ,  "BOS1A04",  1, 2, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 593 
  ChInfo("BOS_3_1" ,  "BOS1A06",  1, 3, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 594 
  ChInfo("BOS_4_1" ,  "BOS1A08",  1, 4, "BOS", 2, 3, 48, 3, 48, 12, 2, 1, 2, 3773, 0),   # 595 
  ChInfo("BOS_5_1" ,  "BOS1A10",  1, 5, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 596 
  ChInfo("BOS_8_1" ,  "BOS1A16",  1, 8, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 597 
  ChInfo("BOS_1_-1",  "BOS1C02", -1, 1, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 598 
  ChInfo("BOS_2_-1",  "BOS1C04", -1, 2, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 599 
  ChInfo("BOS_3_-1",  "BOS1C06", -1, 3, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 600 
  ChInfo("BOS_4_-1",  "BOS1C08", -1, 4, "BOS", 2, 3, 48, 3, 48, 12, 1, 2, 1, 3773, 0),   # 601 
  ChInfo("BOS_5_-1",  "BOS1C10", -1, 5, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 602 
  ChInfo("BOS_8_-1",  "BOS1C16", -1, 8, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 603 
  ChInfo("BOS_1_2" ,  "BOS2A02",  2, 1, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 604 
  ChInfo("BOS_2_2" ,  "BOS2A04",  2, 2, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 605 
  ChInfo("BOS_3_2" ,  "BOS2A06",  2, 3, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 606 
  ChInfo("BOS_4_2" ,  "BOS2A08",  2, 4, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 607 
  ChInfo("BOS_5_2" ,  "BOS2A10",  2, 5, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 608 
  ChInfo("BOS_8_2" ,  "BOS2A16",  2, 8, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 609 
  ChInfo("BOS_1_-2",  "BOS2C02", -2, 1, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 610 
  ChInfo("BOS_2_-2",  "BOS2C04", -2, 2, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 611 
  ChInfo("BOS_3_-2",  "BOS2C06", -2, 3, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 612 
  ChInfo("BOS_4_-2",  "BOS2C08", -2, 4, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 613 
  ChInfo("BOS_5_-2",  "BOS2C10", -2, 5, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 614 
  ChInfo("BOS_8_-2",  "BOS2C16", -2, 8, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 615 
  ChInfo("BOS_1_3" ,  "BOS3A02",  3, 1, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 616 
  ChInfo("BOS_2_3" ,  "BOS3A04",  3, 2, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 617 
  ChInfo("BOS_3_3" ,  "BOS3A06",  3, 3, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 618 
  ChInfo("BOS_4_3" ,  "BOS3A08",  3, 4, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 619 
  ChInfo("BOS_5_3" ,  "BOS3A10",  3, 5, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 620 
  ChInfo("BOS_8_3" ,  "BOS3A16",  3, 8, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 621 
  ChInfo("BOS_1_-3",  "BOS3C02", -3, 1, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 622 
  ChInfo("BOS_2_-3",  "BOS3C04", -3, 2, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 623 
  ChInfo("BOS_3_-3",  "BOS3C06", -3, 3, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 624 
  ChInfo("BOS_4_-3",  "BOS3C08", -3, 4, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 625 
  ChInfo("BOS_5_-3",  "BOS3C10", -3, 5, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 626 
  ChInfo("BOS_8_-3",  "BOS3C16", -3, 8, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 627 
  ChInfo("BOS_1_4" ,  "BOS4A02",  4, 1, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 628 
  ChInfo("BOS_2_4" ,  "BOS4A04",  4, 2, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 629 
  ChInfo("BOS_3_4" ,  "BOS4A06",  4, 3, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 630 
  ChInfo("BOS_4_4" ,  "BOS4A08",  4, 4, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 631 
  ChInfo("BOS_5_4" ,  "BOS4A10",  4, 5, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 632 
  ChInfo("BOS_8_4" ,  "BOS4A16",  4, 8, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 633 
  ChInfo("BOS_1_-4",  "BOS4C02", -4, 1, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 634 
  ChInfo("BOS_2_-4",  "BOS4C04", -4, 2, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 635 
  ChInfo("BOS_3_-4",  "BOS4C06", -4, 3, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 636 
  ChInfo("BOS_4_-4",  "BOS4C08", -4, 4, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 637 
  ChInfo("BOS_5_-4",  "BOS4C10", -4, 5, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 638 
  ChInfo("BOS_8_-4",  "BOS4C16", -4, 8, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 639 
  ChInfo("BOS_1_5" ,  "BOS5A02",  5, 1, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 640 
  ChInfo("BOS_2_5" ,  "BOS5A04",  5, 2, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 641 
  ChInfo("BOS_3_5" ,  "BOS5A06",  5, 3, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 642 
  ChInfo("BOS_4_5" ,  "BOS5A08",  5, 4, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 643 
  ChInfo("BOS_5_5" ,  "BOS5A10",  5, 5, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 644 
  ChInfo("BOS_8_5" ,  "BOS5A16",  5, 8, "BOS", 2, 3, 72, 3, 72, 18, 2, 1, 2, 3773, 0),   # 645 
  ChInfo("BOS_1_-5",  "BOS5C02", -5, 1, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 646 
  ChInfo("BOS_2_-5",  "BOS5C04", -5, 2, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 647 
  ChInfo("BOS_3_-5",  "BOS5C06", -5, 3, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 648 
  ChInfo("BOS_4_-5",  "BOS5C08", -5, 4, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 649 
  ChInfo("BOS_5_-5",  "BOS5C10", -5, 5, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 650 
  ChInfo("BOS_8_-5",  "BOS5C16", -5, 8, "BOS", 2, 3, 72, 3, 72, 18, 1, 2, 1, 3773, 0),   # 651 
  ChInfo("BOS_1_6" ,  "BOS6A02",  6, 1, "BOS", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 652 
  ChInfo("BOS_2_6" ,  "BOS6A04",  6, 2, "BOS", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 653 
  ChInfo("BOS_3_6" ,  "BOS6A06",  6, 3, "BOS", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 654 
  ChInfo("BOS_4_6" ,  "BOS6A08",  6, 4, "BOS", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 655 
  ChInfo("BOS_5_6" ,  "BOS6A10",  6, 5, "BOS", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 656 
  ChInfo("BOS_8_6" ,  "BOS6A16",  6, 8, "BOS", 2, 3, 64, 3, 64, 16, 2, 1, 2, 3773, 0),   # 657 
  ChInfo("BOS_1_-6",  "BOS6C02", -6, 1, "BOS", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 658 
  ChInfo("BOS_2_-6",  "BOS6C04", -6, 2, "BOS", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 659 
  ChInfo("BOS_3_-6",  "BOS6C06", -6, 3, "BOS", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 660 
  ChInfo("BOS_4_-6",  "BOS6C08", -6, 4, "BOS", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 661 
  ChInfo("BOS_5_-6",  "BOS6C10", -6, 5, "BOS", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 662 
  ChInfo("BOS_8_-6",  "BOS6C16", -6, 8, "BOS", 2, 3, 64, 3, 64, 16, 1, 2, 1, 3773, 0),   # 663 
  ChInfo("EEL_1_1" ,  "EEL1A01",  1, 1, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 664 
  ChInfo("EEL_2_1" ,  "EEL1A03",  1, 2, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 665 
  ChInfo("EEL_4_1" ,  "EEL1A07",  1, 4, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 666 
  ChInfo("EEL_5_1" ,  "EEL1A09",  1, 5, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 667 
  ChInfo("EEL_6_1" ,  "EEL1A11",  1, 6, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 668 
  ChInfo("EEL_7_1" ,  "EEL1A13",  1, 7, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 669 
  ChInfo("EEL_8_1" ,  "EEL1A15",  1, 8, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 3842, 0),   # 670 
  ChInfo("EEL_1_-1",  "EEL1C01", -1, 1, "EEL", 2, 3, 40, 3, 40, 10, 2, 1, 2, 3842, 0),   # 671 
  ChInfo("EEL_2_-1",  "EEL1C03", -1, 2, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3842, 0),   # 672 
  ChInfo("EEL_4_-1",  "EEL1C07", -1, 4, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3842, 0),   # 673 
  ChInfo("EEL_5_-1",  "EEL1C09", -1, 5, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3842, 0),   # 674 
  ChInfo("EEL_6_-1",  "EEL1C11", -1, 6, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3842, 0),   # 675 
  ChInfo("EEL_7_-1",  "EEL1C13", -1, 7, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3842, 0),   # 676 
  ChInfo("EEL_8_-1",  "EEL1C15", -1, 8, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 3842, 0),   # 677 
  ChInfo("EEL_1_2" ,  "EEL2A01",  2, 1, "EEL", 2, 3, 40, 3, 40, 10, 1, 2, 1, 4442, 0),   # 678 
  ChInfo("EEL_2_2" ,  "EEL2A03",  2, 2, "EEL", 2, 3, 40, 3, 40, 10, 1, 2, 1, 4442, 0),   # 679 
  ChInfo("EEL_3_1" ,  "EEL2A05",  1, 3, "EEL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4442, 0),   # 680 
  ChInfo("EEL_4_2" ,  "EEL2A07",  2, 4, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 4442, 0),   # 681 
  ChInfo("EEL_5_2" ,  "EEL2A09",  2, 5, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 4442, 0),   # 682 
  ChInfo("EEL_6_2" ,  "EEL2A11",  2, 6, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 4442, 0),   # 683 
  ChInfo("EEL_7_2" ,  "EEL2A13",  2, 7, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 4442, 0),   # 684 
  ChInfo("EEL_8_2" ,  "EEL2A15",  2, 8, "EEL", 2, 3, 40, 3, 40, 10, 2, 2, 1, 4442, 0),   # 685 
  ChInfo("EEL_1_-2",  "EEL2C01", -2, 1, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 686 
  ChInfo("EEL_2_-2",  "EEL2C03", -2, 2, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 687 
  ChInfo("EEL_3_-1",  "EEL2C05", -1, 3, "EEL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4442, 0),   # 688 
  ChInfo("EEL_4_-2",  "EEL2C07", -2, 4, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 689 
  ChInfo("EEL_5_-2",  "EEL2C09", -2, 5, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 690 
  ChInfo("EEL_6_-2",  "EEL2C11", -2, 6, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 691 
  ChInfo("EEL_7_-2",  "EEL2C13", -2, 7, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 692 
  ChInfo("EEL_8_-2",  "EEL2C15", -2, 8, "EEL", 2, 3, 40, 3, 40, 10, 1, 1, 2, 4442, 0),   # 693 
  ChInfo("EES_1_1" ,  "EES1A02",  1, 1, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 694 
  ChInfo("EES_2_1" ,  "EES1A04",  1, 2, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 695 
  ChInfo("EES_3_1" ,  "EES1A06",  1, 3, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 696 
  ChInfo("EES_4_1" ,  "EES1A08",  1, 4, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 697 
  ChInfo("EES_5_1" ,  "EES1A10",  1, 5, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 698 
  ChInfo("EES_6_1" ,  "EES1A12",  1, 6, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 699 
  ChInfo("EES_7_1" ,  "EES1A14",  1, 7, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 700 
  ChInfo("EES_8_1" ,  "EES1A16",  1, 8, "EES", 2, 3, 48, 3, 48, 12, 1, 1, 2, 2375, 0),   # 701 
  ChInfo("EES_1_-1" , "EES1C02", -1, 1, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 702 
  ChInfo("EES_2_-1" , "EES1C04", -1, 2, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 703 
  ChInfo("EES_3_-1" , "EES1C06", -1, 3, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 704 
  ChInfo("EES_4_-1" , "EES1C08", -1, 4, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 705 
  ChInfo("EES_5_-1" , "EES1C10", -1, 5, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 706 
  ChInfo("EES_6_-1" , "EES1C12", -1, 6, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 707 
  ChInfo("EES_7_-1" , "EES1C14", -1, 7, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 708 
  ChInfo("EES_8_-1" , "EES1C16", -1, 8, "EES", 2, 3, 48, 3, 48, 12, 2, 2, 1, 2375, 0),   # 709 
  ChInfo("EES_1_2" ,  "EES2A02",  2, 1, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 710 
  ChInfo("EES_2_2" ,  "EES2A04",  2, 2, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 711 
  ChInfo("EES_3_2" ,  "EES2A06",  2, 3, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 712 
  ChInfo("EES_4_2" ,  "EES2A08",  2, 4, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 713 
  ChInfo("EES_5_2" ,  "EES2A10",  2, 5, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 714 
  ChInfo("EES_6_2" ,  "EES2A12",  2, 6, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 715 
  ChInfo("EES_7_2" ,  "EES2A14",  2, 7, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 716 
  ChInfo("EES_8_2" ,  "EES2A16",  2, 8, "EES", 2, 3, 40, 3, 40, 10, 1, 1, 2, 2375, 0),   # 717 
  ChInfo("EES_1_-2" , "EES2C02", -2, 1, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 718 
  ChInfo("EES_2_-2" , "EES2C04", -2, 2, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 719 
  ChInfo("EES_3_-2" , "EES2C06", -2, 3, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 720 
  ChInfo("EES_4_-2" , "EES2C08", -2, 4, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 721 
  ChInfo("EES_5_-2" , "EES2C10", -2, 5, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 722 
  ChInfo("EES_6_-2" , "EES2C12", -2, 6, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 723 
  ChInfo("EES_7_-2" , "EES2C14", -2, 7, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 724 
  ChInfo("EES_8_-2" , "EES2C16", -2, 8, "EES", 2, 3, 40, 3, 40, 10, 2, 2, 1, 2375, 0),   # 725 
  ChInfo("EIL_1_4" ,  "EIL4A01",  4, 1, "EIL", 2, 4, 12, 4, 12,  4, 1, 5, 6, 3072, 0),   # 726 
  ChInfo("EIL_2_4" ,  "EIL4A03",  4, 2, "EIL", 2, 4, 54, 4, 54, 18, 1, 4, 3, 3072, 0),   # 727 
  ChInfo("EIL_3_4" ,  "EIL4A05",  4, 3, "EIL", 2, 4, 54, 4, 54, 18, 1, 4, 3, 3072, 0),   # 728 
  ChInfo("EIL_4_4" ,  "EIL4A07",  4, 4, "EIL", 2, 4, 54, 4, 54, 18, 1, 4, 3, 2372, 0),   # 739 
  ChInfo("EIL_5_4" ,  "EIL4A09",  4, 5, "EIL", 2, 4, 12, 4, 12, 4 , 1, 5, 6, 3072, 0),   # 730 
  ChInfo("EIL_6_4" ,  "EIL4A11",  4, 6, "EIL", 2, 4, 42, 4, 42, 14, 1, 4, 3, 1822, 0),   # 731 
  ChInfo("EIL_7_4" ,  "EIL4A13",  4, 7, "EIL", 2, 4, 54, 4, 54, 18, 1, 4, 3, 3072, 0),   # 732 
  ChInfo("EIL_8_4" ,  "EIL4A15",  4, 8, "EIL", 2, 4, 42, 4, 42, 14, 1, 4, 3, 1822, 0),   # 733 
  ChInfo("EIL_1_-4",  "EIL4C01", -4, 1, "EIL", 2, 4, 12, 4, 12, 4 , 2, 5, 6, 3072, 0),   # 734 
  ChInfo("EIL_2_-4",  "EIL4C03", -4, 2, "EIL", 2, 4, 54, 4, 54, 18, 2, 3, 4, 3072, 0),   # 735 
  ChInfo("EIL_3_-4",  "EIL4C05", -4, 3, "EIL", 2, 4, 54, 4, 54, 18, 2, 3, 4, 3072, 0),   # 736 
  ChInfo("EIL_4_-4",  "EIL4C07", -4, 4, "EIL", 2, 4, 54, 4, 54, 18, 2, 3, 4, 2372, 0),   # 737 
  ChInfo("EIL_5_-4",  "EIL4C09", -4, 5, "EIL", 2, 4, 12, 4, 12, 4 , 2, 5, 6, 3072, 0),   # 738 
  ChInfo("EIL_6_-4",  "EIL4C11", -4, 6, "EIL", 2, 4, 42, 4, 42, 14, 2, 3, 4, 1822, 0),   # 739 
  ChInfo("EIL_7_-4",  "EIL4C13", -4, 7, "EIL", 2, 4, 54, 4, 54, 18, 2, 3, 4, 3072, 0),   # 740 
  ChInfo("EIL_8_-4",  "EIL4C15", -4, 8, "EIL", 2, 4, 42, 4, 42, 14, 2, 3, 4, 1822, 0),   # 741 
  ChInfo("EIL_1_5" ,  "EIL5A01",  5, 1, "EIL", 2, 4, 42, 4, 42, 14, 1, 4, 3, 2675, 0),   # 742 
  ChInfo("EIL_5_5" ,  "EIL5A09",  5, 5, "EIL", 2, 4, 42, 4, 42, 14, 1, 4, 3, 2750, 0),   # 743 
  ChInfo("EIL_1_-5",  "EIL5C01", -5, 1, "EIL", 2, 4, 42, 4, 42, 14, 2, 3, 4, 3025, 0),   # 744 
  ChInfo("EIL_5_-5",  "EIL5C09", -5, 5, "EIL", 2, 4, 42, 4, 42, 14, 2, 3, 4, 3025, 0),   # 745 
  ChInfo("EML_1_1" ,  "EML1A01",  1, 1, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 746 
  ChInfo("EML_2_1" ,  "EML1A03",  1, 2, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 747 
  ChInfo("EML_3_1" ,  "EML1A05",  1, 3, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 748 
  ChInfo("EML_4_1" ,  "EML1A07",  1, 4, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 749 
  ChInfo("EML_5_1" ,  "EML1A09",  1, 5, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 750 
  ChInfo("EML_6_1" ,  "EML1A11",  1, 6, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 751 
  ChInfo("EML_7_1" ,  "EML1A13",  1, 7, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 752 
  ChInfo("EML_8_1" ,  "EML1A15",  1, 8, "EML", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1907, 0),   # 753 
  ChInfo("EML_1_-1",  "EML1C01", -1, 1, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 754 
  ChInfo("EML_2_-1",  "EML1C03", -1, 2, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 755 
  ChInfo("EML_3_-1",  "EML1C05", -1, 3, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 756 
  ChInfo("EML_4_-1",  "EML1C07", -1, 4, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 757 
  ChInfo("EML_5_-1",  "EML1C09", -1, 5, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 758 
  ChInfo("EML_6_-1",  "EML1C11", -1, 6, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 759 
  ChInfo("EML_7_-1",  "EML1C13", -1, 7, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 760 
  ChInfo("EML_8_-1",  "EML1C15", -1, 8, "EML", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1907, 0),   # 761 
  ChInfo("EML_1_2" ,  "EML2A01",  2, 1, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 762 
  ChInfo("EML_2_2" ,  "EML2A03",  2, 2, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 763 
  ChInfo("EML_3_2" ,  "EML2A05",  2, 3, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 764 
  ChInfo("EML_4_2" ,  "EML2A07",  2, 4, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 765 
  ChInfo("EML_5_2" ,  "EML2A09",  2, 5, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 766 
  ChInfo("EML_6_2" ,  "EML2A11",  2, 6, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 767 
  ChInfo("EML_7_2" ,  "EML2A13",  2, 7, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 768 
  ChInfo("EML_8_2" ,  "EML2A15",  2, 8, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2867, 0),   # 769 
  ChInfo("EML_1_-2",  "EML2C01", -2, 1, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 770 
  ChInfo("EML_2_-2",  "EML2C03", -2, 2, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 771 
  ChInfo("EML_3_-2",  "EML2C05", -2, 3, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 772 
  ChInfo("EML_4_-2",  "EML2C07", -2, 4, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 773 
  ChInfo("EML_5_-2",  "EML2C09", -2, 5, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 774 
  ChInfo("EML_6_-2",  "EML2C11", -2, 6, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 775 
  ChInfo("EML_7_-2",  "EML2C13", -2, 7, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 776 
  ChInfo("EML_8_-2",  "EML2C15", -2, 8, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2867, 0),   # 777 
  ChInfo("EML_1_3" ,  "EML3A01",  3, 1, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 778 
  ChInfo("EML_2_3" ,  "EML3A03",  3, 2, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 779 
  ChInfo("EML_3_3" ,  "EML3A05",  3, 3, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 780 
  ChInfo("EML_4_3" ,  "EML3A07",  3, 4, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 781 
  ChInfo("EML_5_3" ,  "EML3A09",  3, 5, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 782 
  ChInfo("EML_6_3" ,  "EML3A11",  3, 6, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 783 
  ChInfo("EML_7_3" ,  "EML3A13",  3, 7, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 784 
  ChInfo("EML_8_3" ,  "EML3A15",  3, 8, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3827, 0),   # 785 
  ChInfo("EML_1_-3",  "EML3C01", -3, 1, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 786 
  ChInfo("EML_2_-3",  "EML3C03", -3, 2, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 787 
  ChInfo("EML_3_-3",  "EML3C05", -3, 3, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 788 
  ChInfo("EML_4_-3",  "EML3C07", -3, 4, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 789 
  ChInfo("EML_5_-3",  "EML3C09", -3, 5, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 790 
  ChInfo("EML_6_-3",  "EML3C11", -3, 6, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 791 
  ChInfo("EML_7_-3",  "EML3C13", -3, 7, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 792 
  ChInfo("EML_8_-3",  "EML3C15", -3, 8, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3827, 0),   # 793 
  ChInfo("EML_1_4" ,  "EML4A01",  4, 1, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 794 
  ChInfo("EML_2_4" ,  "EML4A03",  4, 2, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 795 
  ChInfo("EML_3_4" ,  "EML4A05",  4, 3, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 796 
  ChInfo("EML_4_4" ,  "EML4A07",  4, 4, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 797 
  ChInfo("EML_5_4" ,  "EML4A09",  4, 5, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 798 
  ChInfo("EML_6_4" ,  "EML4A11",  4, 6, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 799 
  ChInfo("EML_7_4" ,  "EML4A13",  4, 7, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 800 
  ChInfo("EML_8_4" ,  "EML4A15",  4, 8, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 4787, 0),   # 801 
  ChInfo("EML_1_-4",  "EML4C01", -4, 1, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 802 
  ChInfo("EML_2_-4",  "EML4C03", -4, 2, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 803 
  ChInfo("EML_3_-4",  "EML4C05", -4, 3, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 804 
  ChInfo("EML_4_-4",  "EML4C07", -4, 4, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 805 
  ChInfo("EML_5_-4",  "EML4C09", -4, 5, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 806 
  ChInfo("EML_6_-4",  "EML4C11", -4, 6, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 807 
  ChInfo("EML_7_-4",  "EML4C13", -4, 7, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 808 
  ChInfo("EML_8_-4",  "EML4C15", -4, 8, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 4787, 0),   # 809 
  ChInfo("EML_1_5" ,  "EML5A01",  5, 1, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 810 
  ChInfo("EML_2_5" ,  "EML5A03",  5, 2, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 811 
  ChInfo("EML_3_5" ,  "EML5A05",  5, 3, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 812 
  ChInfo("EML_4_5" ,  "EML5A07",  5, 4, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 813 
  ChInfo("EML_5_5" ,  "EML5A09",  5, 5, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 814 
  ChInfo("EML_6_5" ,  "EML5A11",  5, 6, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 815 
  ChInfo("EML_7_5" ,  "EML5A13",  5, 7, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 816 
  ChInfo("EML_8_5" ,  "EML5A15",  5, 8, "EML", 2, 3, 64, 3, 64, 16, 2, 2, 1, 5747, 0),   # 817 
  ChInfo("EML_1_-5",  "EML5C01", -5, 1, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 818 
  ChInfo("EML_2_-5",  "EML5C03", -5, 2, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 819 
  ChInfo("EML_3_-5",  "EML5C05", -5, 3, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 820 
  ChInfo("EML_4_-5",  "EML5C07", -5, 4, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 821 
  ChInfo("EML_5_-5",  "EML5C09", -5, 5, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 822 
  ChInfo("EML_6_-5",  "EML5C11", -5, 6, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 823 
  ChInfo("EML_7_-5",  "EML5C13", -5, 7, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 824 
  ChInfo("EML_8_-5",  "EML5C15", -5, 8, "EML", 2, 3, 64, 3, 64, 16, 1, 1, 2, 5747, 0),   # 825 
  ChInfo("EMS_1_1" ,  "EMS1A02",  1, 1, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 826 
  ChInfo("EMS_2_1" ,  "EMS1A04",  1, 2, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 827 
  ChInfo("EMS_3_1" ,  "EMS1A06",  1, 3, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 828 
  ChInfo("EMS_4_1" ,  "EMS1A08",  1, 4, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 829 
  ChInfo("EMS_5_1" ,  "EMS1A10",  1, 5, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 830 
  ChInfo("EMS_6_1" ,  "EMS1A12",  1, 6, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 831 
  ChInfo("EMS_7_1" ,  "EMS1A14",  1, 7, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 832 
  ChInfo("EMS_8_1" ,  "EMS1A16",  1, 8, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1340, 0),   # 833 
  ChInfo("EMS_1_-1",  "EMS1C02", -1, 1, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 834 
  ChInfo("EMS_2_-1",  "EMS1C04", -1, 2, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 835 
  ChInfo("EMS_3_-1",  "EMS1C06", -1, 3, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 836 
  ChInfo("EMS_4_-1",  "EMS1C08", -1, 4, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 837 
  ChInfo("EMS_5_-1",  "EMS1C10", -1, 5, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 838 
  ChInfo("EMS_6_-1",  "EMS1C12", -1, 6, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 839 
  ChInfo("EMS_7_-1",  "EMS1C14", -1, 7, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 840 
  ChInfo("EMS_8_-1",  "EMS1C16", -1, 8, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1340, 0),   # 841 
  ChInfo("EMS_1_2" ,  "EMS2A02",  2, 1, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 842 
  ChInfo("EMS_2_2" ,  "EMS2A04",  2, 2, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 843 
  ChInfo("EMS_3_2" ,  "EMS2A06",  2, 3, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 844 
  ChInfo("EMS_4_2" ,  "EMS2A08",  2, 4, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 845 
  ChInfo("EMS_5_2" ,  "EMS2A10",  2, 5, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 846 
  ChInfo("EMS_6_2" ,  "EMS2A12",  2, 6, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 847 
  ChInfo("EMS_7_2" ,  "EMS2A14",  2, 7, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 848 
  ChInfo("EMS_8_2" ,  "EMS2A16",  2, 8, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 1916, 0),   # 849 
  ChInfo("EMS_1_-2",  "EMS2C02", -2, 1, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 850 
  ChInfo("EMS_2_-2",  "EMS2C04", -2, 2, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 851 
  ChInfo("EMS_3_-2",  "EMS2C06", -2, 3, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 852 
  ChInfo("EMS_4_-2",  "EMS2C08", -2, 4, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 853 
  ChInfo("EMS_5_-2",  "EMS2C10", -2, 5, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 854 
  ChInfo("EMS_6_-2",  "EMS2C12", -2, 6, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 855 
  ChInfo("EMS_7_-2",  "EMS2C14", -2, 7, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 856 
  ChInfo("EMS_8_-2",  "EMS2C16", -2, 8, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 1916, 0),   # 857 
  ChInfo("EMS_1_3" ,  "EMS3A02",  3, 1, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 858 
  ChInfo("EMS_2_3" ,  "EMS3A04",  3, 2, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 859 
  ChInfo("EMS_3_3" ,  "EMS3A06",  3, 3, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 860 
  ChInfo("EMS_4_3" ,  "EMS3A08",  3, 4, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 861 
  ChInfo("EMS_5_3" ,  "EMS3A10",  3, 5, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 862 
  ChInfo("EMS_6_3" ,  "EMS3A12",  3, 6, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 863 
  ChInfo("EMS_7_3" ,  "EMS3A14",  3, 7, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 864 
  ChInfo("EMS_8_3" ,  "EMS3A16",  3, 8, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 2492, 0),   # 865 
  ChInfo("EMS_1_-3",  "EMS3C02", -3, 1, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 866 
  ChInfo("EMS_2_-3",  "EMS3C04", -3, 2, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 867 
  ChInfo("EMS_3_-3",  "EMS3C06", -3, 3, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 868 
  ChInfo("EMS_4_-3",  "EMS3C08", -3, 4, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 869 
  ChInfo("EMS_5_-3",  "EMS3C10", -3, 5, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 870 
  ChInfo("EMS_6_-3",  "EMS3C12", -3, 6, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 871 
  ChInfo("EMS_7_-3",  "EMS3C14", -3, 7, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 872 
  ChInfo("EMS_8_-3",  "EMS3C16", -3, 8, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 2492, 0),   # 873 
  ChInfo("EMS_1_4" ,  "EMS4A02",  4, 1, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 874 
  ChInfo("EMS_2_4" ,  "EMS4A04",  4, 2, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 875 
  ChInfo("EMS_3_4" ,  "EMS4A06",  4, 3, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 876 
  ChInfo("EMS_4_4" ,  "EMS4A08",  4, 4, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 877 
  ChInfo("EMS_5_4" ,  "EMS4A10",  4, 5, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 878 
  ChInfo("EMS_6_4" ,  "EMS4A12",  4, 6, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 879 
  ChInfo("EMS_7_4" ,  "EMS4A14",  4, 7, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 880 
  ChInfo("EMS_8_4" ,  "EMS4A16",  4, 8, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3068, 0),   # 881 
  ChInfo("EMS_1_-4",  "EMS4C02", -4, 1, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 882 
  ChInfo("EMS_2_-4",  "EMS4C04", -4, 2, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 883 
  ChInfo("EMS_3_-4",  "EMS4C06", -4, 3, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 884 
  ChInfo("EMS_4_-4",  "EMS4C08", -4, 4, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 885 
  ChInfo("EMS_5_-4",  "EMS4C10", -4, 5, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 886 
  ChInfo("EMS_6_-4",  "EMS4C12", -4, 6, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 887 
  ChInfo("EMS_7_-4",  "EMS4C14", -4, 7, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 888 
  ChInfo("EMS_8_-4",  "EMS4C16", -4, 8, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3068, 0),   # 889 
  ChInfo("EMS_1_5" ,  "EMS5A02",  5, 1, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 890 
  ChInfo("EMS_2_5" ,  "EMS5A04",  5, 2, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 891 
  ChInfo("EMS_3_5" ,  "EMS5A06",  5, 3, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 892 
  ChInfo("EMS_4_5" ,  "EMS5A08",  5, 4, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 893 
  ChInfo("EMS_5_5" ,  "EMS5A10",  5, 5, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 894 
  ChInfo("EMS_6_5" ,  "EMS5A12",  5, 6, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 895 
  ChInfo("EMS_7_5" ,  "EMS5A14",  5, 7, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 896 
  ChInfo("EMS_8_5" ,  "EMS5A16",  5, 8, "EMS", 2, 3, 64, 3, 64, 16, 1, 1, 2, 3644, 0),   # 897 
  ChInfo("EMS_1_-5",  "EMS5C02", -5, 1, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 898 
  ChInfo("EMS_2_-5",  "EMS5C04", -5, 2, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 899 
  ChInfo("EMS_3_-5",  "EMS5C06", -5, 3, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 900 
  ChInfo("EMS_4_-5",  "EMS5C08", -5, 4, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 901 
  ChInfo("EMS_5_-5",  "EMS5C10", -5, 5, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 902 
  ChInfo("EMS_6_-5",  "EMS5C12", -5, 6, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 903 
  ChInfo("EMS_7_-5",  "EMS5C14", -5, 7, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 904 
  ChInfo("EMS_8_-5",  "EMS5C16", -5, 8, "EMS", 2, 3, 64, 3, 64, 16, 2, 2, 1, 3644, 0),   # 905 
  ChInfo("EOL_1_1" ,  "EOL1A01",  1, 1, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 906 
  ChInfo("EOL_2_1" ,  "EOL1A03",  1, 2, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 907 
  ChInfo("EOL_3_1" ,  "EOL1A05",  1, 3, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 908 
  ChInfo("EOL_4_1" ,  "EOL1A07",  1, 4, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 909 
  ChInfo("EOL_5_1" ,  "EOL1A09",  1, 5, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 910 
  ChInfo("EOL_6_1" ,  "EOL1A11",  1, 6, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 911 
  ChInfo("EOL_7_1" ,  "EOL1A13",  1, 7, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 912 
  ChInfo("EOL_8_1" ,  "EOL1A15",  1, 8, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2402, 0),   # 913 
  ChInfo("EOL_1_-1",  "EOL1C01", -1, 1, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 914 
  ChInfo("EOL_2_-1",  "EOL1C03", -1, 2, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 915 
  ChInfo("EOL_3_-1",  "EOL1C05", -1, 3, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 916 
  ChInfo("EOL_4_-1",  "EOL1C07", -1, 4, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 917 
  ChInfo("EOL_5_-1",  "EOL1C09", -1, 5, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 918 
  ChInfo("EOL_6_-1",  "EOL1C11", -1, 6, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 919 
  ChInfo("EOL_7_-1",  "EOL1C13", -1, 7, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 920 
  ChInfo("EOL_8_-1",  "EOL1C15", -1, 8, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2402, 0),   # 921 
  ChInfo("EOL_1_2" ,  "EOL2A01",  2, 1, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 922 
  ChInfo("EOL_2_2" ,  "EOL2A03",  2, 2, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 923 
  ChInfo("EOL_3_2" ,  "EOL2A05",  2, 3, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 924 
  ChInfo("EOL_4_2" ,  "EOL2A07",  2, 4, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 925 
  ChInfo("EOL_5_2" ,  "EOL2A09",  2, 5, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 926 
  ChInfo("EOL_6_2" ,  "EOL2A11",  2, 6, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 927 
  ChInfo("EOL_7_2" ,  "EOL2A13",  2, 7, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 928 
  ChInfo("EOL_8_2" ,  "EOL2A15",  2, 8, "EOL", 2, 3, 56, 3, 56, 14, 2, 2, 1, 3362, 0),   # 929 
  ChInfo("EOL_1_-2",  "EOL2C01", -2, 1, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 930 
  ChInfo("EOL_2_-2",  "EOL2C03", -2, 2, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 931 
  ChInfo("EOL_3_-2",  "EOL2C05", -2, 3, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 932 
  ChInfo("EOL_4_-2",  "EOL2C07", -2, 4, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 933 
  ChInfo("EOL_5_-2",  "EOL2C09", -2, 5, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 934 
  ChInfo("EOL_6_-2",  "EOL2C11", -2, 6, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 935 
  ChInfo("EOL_7_-2",  "EOL2C13", -2, 7, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 936 
  ChInfo("EOL_8_-2",  "EOL2C15", -2, 8, "EOL", 2, 3, 56, 3, 56, 14, 1, 1, 2, 3362, 0),   # 937 
  ChInfo("EOL_1_3" ,  "EOL3A01",  3, 1, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 938 
  ChInfo("EOL_2_3" ,  "EOL3A03",  3, 2, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 939 
  ChInfo("EOL_3_3" ,  "EOL3A05",  3, 3, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 940 
  ChInfo("EOL_4_3" ,  "EOL3A07",  3, 4, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 941 
  ChInfo("EOL_5_3" ,  "EOL3A09",  3, 5, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 942 
  ChInfo("EOL_6_3" ,  "EOL3A11",  3, 6, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 943 
  ChInfo("EOL_7_3" ,  "EOL3A13",  3, 7, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 944 
  ChInfo("EOL_8_3" ,  "EOL3A15",  3, 8, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4082, 0),   # 945 
  ChInfo("EOL_1_-3",  "EOL3C01", -3, 1, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 946 
  ChInfo("EOL_2_-3",  "EOL3C03", -3, 2, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 947 
  ChInfo("EOL_3_-3",  "EOL3C05", -3, 3, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 948 
  ChInfo("EOL_4_-3",  "EOL3C07", -3, 4, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 949 
  ChInfo("EOL_5_-3",  "EOL3C09", -3, 5, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 950 
  ChInfo("EOL_6_-3",  "EOL3C11", -3, 6, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 951 
  ChInfo("EOL_7_-3",  "EOL3C13", -3, 7, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 952 
  ChInfo("EOL_8_-3",  "EOL3C15", -3, 8, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4082, 0),   # 953 
  ChInfo("EOL_1_4" ,  "EOL4A01",  4, 1, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 954 
  ChInfo("EOL_2_4" ,  "EOL4A03",  4, 2, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 955 
  ChInfo("EOL_3_4" ,  "EOL4A05",  4, 3, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 956 
  ChInfo("EOL_4_4" ,  "EOL4A07",  4, 4, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 957 
  ChInfo("EOL_5_4" ,  "EOL4A09",  4, 5, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 958 
  ChInfo("EOL_6_4" ,  "EOL4A11",  4, 6, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 959 
  ChInfo("EOL_7_4" ,  "EOL4A13",  4, 7, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 960 
  ChInfo("EOL_8_4" ,  "EOL4A15",  4, 8, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 4802, 0),   # 961 
  ChInfo("EOL_1_-4",  "EOL4C01", -4, 1, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 962 
  ChInfo("EOL_2_-4",  "EOL4C03", -4, 2, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 963 
  ChInfo("EOL_3_-4",  "EOL4C05", -4, 3, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 964 
  ChInfo("EOL_4_-4",  "EOL4C07", -4, 4, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 965 
  ChInfo("EOL_5_-4",  "EOL4C09", -4, 5, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 966 
  ChInfo("EOL_6_-4",  "EOL4C11", -4, 6, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 967 
  ChInfo("EOL_7_-4",  "EOL4C13", -4, 7, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 968 
  ChInfo("EOL_8_-4",  "EOL4C15", -4, 8, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 4802, 0),   # 969 
  ChInfo("EOL_1_5" ,  "EOL5A01",  5, 1, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 970 
  ChInfo("EOL_2_5" ,  "EOL5A03",  5, 2, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 971 
  ChInfo("EOL_3_5" ,  "EOL5A05",  5, 3, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 972 
  ChInfo("EOL_4_5" ,  "EOL5A07",  5, 4, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 973 
  ChInfo("EOL_5_5" ,  "EOL5A09",  5, 5, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 974 
  ChInfo("EOL_6_5" ,  "EOL5A11",  5, 6, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 975 
  ChInfo("EOL_7_5" ,  "EOL5A13",  5, 7, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 976 
  ChInfo("EOL_8_5" ,  "EOL5A15",  5, 8, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 5522, 0),   # 977 
  ChInfo("EOL_1_-5",  "EOL5C01", -5, 1, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 978 
  ChInfo("EOL_2_-5",  "EOL5C03", -5, 2, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 979 
  ChInfo("EOL_3_-5",  "EOL5C05", -5, 3, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 980 
  ChInfo("EOL_4_-5",  "EOL5C07", -5, 4, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 981 
  ChInfo("EOL_5_-5",  "EOL5C09", -5, 5, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 982 
  ChInfo("EOL_6_-5",  "EOL5C11", -5, 6, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 983 
  ChInfo("EOL_7_-5",  "EOL5C13", -5, 7, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 984 
  ChInfo("EOL_8_-5",  "EOL5C15", -5, 8, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 5522, 0),   # 985 
  ChInfo("EOL_1_6" ,  "EOL6A01",  6, 1, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 986 
  ChInfo("EOL_2_6" ,  "EOL6A03",  6, 2, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 987 
  ChInfo("EOL_3_6" ,  "EOL6A05",  6, 3, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 988 
  ChInfo("EOL_4_6" ,  "EOL6A07",  6, 4, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 989 
  ChInfo("EOL_5_6" ,  "EOL6A09",  6, 5, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 990 
  ChInfo("EOL_6_6" ,  "EOL6A11",  6, 6, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 991 
  ChInfo("EOL_7_6" ,  "EOL6A13",  6, 7, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 992 
  ChInfo("EOL_8_6" ,  "EOL6A15",  6, 8, "EOL", 2, 3, 48, 3, 48, 12, 2, 2, 1, 6242, 0),   # 993 
  ChInfo("EOL_1_-6",  "EOL6C01", -6, 1, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 994 
  ChInfo("EOL_2_-6",  "EOL6C03", -6, 2, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 995 
  ChInfo("EOL_3_-6",  "EOL6C05", -6, 3, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 996 
  ChInfo("EOL_4_-6",  "EOL6C07", -6, 4, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 997 
  ChInfo("EOL_5_-6",  "EOL6C09", -6, 5, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 998 
  ChInfo("EOL_6_-6",  "EOL6C11", -6, 6, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 999 
  ChInfo("EOL_7_-6",  "EOL6C13", -6, 7, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 1000
  ChInfo("EOL_8_-6",  "EOL6C15", -6, 8, "EOL", 2, 3, 48, 3, 48, 12, 1, 1, 2, 6242, 0),   # 1001
  ChInfo("EOS_1_1" ,  "EOS1A02",  1, 1, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1002
  ChInfo("EOS_2_1" ,  "EOS1A04",  1, 2, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1003
  ChInfo("EOS_3_1" ,  "EOS1A06",  1, 3, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1004
  ChInfo("EOS_4_1" ,  "EOS1A08",  1, 4, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1005
  ChInfo("EOS_5_1" ,  "EOS1A10",  1, 5, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1006
  ChInfo("EOS_6_1" ,  "EOS1A12",  1, 6, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1007
  ChInfo("EOS_7_1" ,  "EOS1A14",  1, 7, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1008
  ChInfo("EOS_8_1" ,  "EOS1A16",  1, 8, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 1682, 0),   # 1009
  ChInfo("EOS_1_-1",  "EOS1C02", -1, 1, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1010
  ChInfo("EOS_2_-1",  "EOS1C04", -1, 2, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1011
  ChInfo("EOS_3_-1",  "EOS1C06", -1, 3, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1012
  ChInfo("EOS_4_-1",  "EOS1C08", -1, 4, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1013
  ChInfo("EOS_5_-1",  "EOS1C10", -1, 5, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1014
  ChInfo("EOS_6_-1",  "EOS1C12", -1, 6, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1015
  ChInfo("EOS_7_-1",  "EOS1C14", -1, 7, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1016
  ChInfo("EOS_8_-1",  "EOS1C16", -1, 8, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 1682, 0),   # 1017
  ChInfo("EOS_1_2" ,  "EOS2A02",  2, 1, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1018
  ChInfo("EOS_2_2" ,  "EOS2A04",  2, 2, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1019
  ChInfo("EOS_3_2" ,  "EOS2A06",  2, 3, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1020
  ChInfo("EOS_4_2" ,  "EOS2A08",  2, 4, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1021
  ChInfo("EOS_5_2" ,  "EOS2A10",  2, 5, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1022
  ChInfo("EOS_6_2" ,  "EOS2A12",  2, 6, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1023
  ChInfo("EOS_7_2" ,  "EOS2A14",  2, 7, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1024
  ChInfo("EOS_8_2" ,  "EOS2A16",  2, 8, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2186, 0),   # 1025
  ChInfo("EOS_1_-2",  "EOS2C02", -2, 1, "EOS", 2, 3, 56, 3, 56, 14, 1, 2, 1, 2186, 0),   # 1026
  ChInfo("EOS_2_-2",  "EOS2C04", -2, 2, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1027
  ChInfo("EOS_3_-2",  "EOS2C06", -2, 3, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1028
  ChInfo("EOS_4_-2",  "EOS2C08", -2, 4, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1029
  ChInfo("EOS_5_-2",  "EOS2C10", -2, 5, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1030
  ChInfo("EOS_6_-2",  "EOS2C12", -2, 6, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1031
  ChInfo("EOS_7_-2",  "EOS2C14", -2, 7, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1032
  ChInfo("EOS_8_-2",  "EOS2C16", -2, 8, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2186, 0),   # 1033
  ChInfo("EOS_1_3" ,  "EOS3A02",  3, 1, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1034
  ChInfo("EOS_2_3" ,  "EOS3A04",  3, 2, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1035
  ChInfo("EOS_3_3" ,  "EOS3A06",  3, 3, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1036
  ChInfo("EOS_4_3" ,  "EOS3A08",  3, 4, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1037
  ChInfo("EOS_5_3" ,  "EOS3A10",  3, 5, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1038
  ChInfo("EOS_6_3" ,  "EOS3A12",  3, 6, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1039
  ChInfo("EOS_7_3" ,  "EOS3A14",  3, 7, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1040
  ChInfo("EOS_8_3" ,  "EOS3A16",  3, 8, "EOS", 2, 3, 56, 3, 56, 14, 1, 1, 2, 2690, 0),   # 1041
  ChInfo("EOS_1_-3",  "EOS3C02", -3, 1, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1042
  ChInfo("EOS_2_-3",  "EOS3C04", -3, 2, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1043
  ChInfo("EOS_3_-3",  "EOS3C06", -3, 3, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1044
  ChInfo("EOS_4_-3",  "EOS3C08", -3, 4, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1045
  ChInfo("EOS_5_-3",  "EOS3C10", -3, 5, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1046
  ChInfo("EOS_6_-3",  "EOS3C12", -3, 6, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1047
  ChInfo("EOS_7_-3",  "EOS3C14", -3, 7, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1048
  ChInfo("EOS_8_-3",  "EOS3C16", -3, 8, "EOS", 2, 3, 56, 3, 56, 14, 2, 2, 1, 2690, 0),   # 1049
  ChInfo("EOS_1_4" ,  "EOS4A02",  4, 1, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1050
  ChInfo("EOS_2_4" ,  "EOS4A04",  4, 2, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1051
  ChInfo("EOS_3_4" ,  "EOS4A06",  4, 3, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1052
  ChInfo("EOS_4_4" ,  "EOS4A08",  4, 4, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1053
  ChInfo("EOS_5_4" ,  "EOS4A10",  4, 5, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1054
  ChInfo("EOS_6_4" ,  "EOS4A12",  4, 6, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1055
  ChInfo("EOS_7_4" ,  "EOS4A14",  4, 7, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1056
  ChInfo("EOS_8_4" ,  "EOS4A16",  4, 8, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3122, 0),   # 1057
  ChInfo("EOS_1_-4",  "EOS4C02", -4, 1, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1058
  ChInfo("EOS_2_-4",  "EOS4C04", -4, 2, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1059
  ChInfo("EOS_3_-4",  "EOS4C06", -4, 3, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1060
  ChInfo("EOS_4_-4",  "EOS4C08", -4, 4, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1061
  ChInfo("EOS_5_-4",  "EOS4C10", -4, 5, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1062
  ChInfo("EOS_6_-4",  "EOS4C12", -4, 6, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1063
  ChInfo("EOS_7_-4",  "EOS4C14", -4, 7, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1064
  ChInfo("EOS_8_-4",  "EOS4C16", -4, 8, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3122, 0),   # 1065
  ChInfo("EOS_1_5" ,  "EOS5A02",  5, 1, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1066
  ChInfo("EOS_2_5" ,  "EOS5A04",  5, 2, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1067
  ChInfo("EOS_3_5" ,  "EOS5A06",  5, 3, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1068
  ChInfo("EOS_4_5" ,  "EOS5A08",  5, 4, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1069
  ChInfo("EOS_5_5" ,  "EOS5A10",  5, 5, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1070
  ChInfo("EOS_6_5" ,  "EOS5A12",  5, 6, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1071
  ChInfo("EOS_7_5" ,  "EOS5A14",  5, 7, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1072
  ChInfo("EOS_8_5" ,  "EOS5A16",  5, 8, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3554, 0),   # 1073
  ChInfo("EOS_1_-5",  "EOS5C02", -5, 1, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1074
  ChInfo("EOS_2_-5",  "EOS5C04", -5, 2, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1075
  ChInfo("EOS_3_-5",  "EOS5C06", -5, 3, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1076
  ChInfo("EOS_4_-5",  "EOS5C08", -5, 4, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1077
  ChInfo("EOS_5_-5",  "EOS5C10", -5, 5, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1078
  ChInfo("EOS_6_-5",  "EOS5C12", -5, 6, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1079
  ChInfo("EOS_7_-5",  "EOS5C14", -5, 7, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1080
  ChInfo("EOS_8_-5",  "EOS5C16", -5, 8, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3554, 0),   # 1081
  ChInfo("EOS_1_6" ,  "EOS6A02",  6, 1, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1082
  ChInfo("EOS_2_6" ,  "EOS6A04",  6, 2, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1083
  ChInfo("EOS_3_6" ,  "EOS6A06",  6, 3, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1084
  ChInfo("EOS_4_6" ,  "EOS6A08",  6, 4, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1085
  ChInfo("EOS_5_6" ,  "EOS6A10",  6, 5, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1086
  ChInfo("EOS_6_6" ,  "EOS6A12",  6, 6, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1087
  ChInfo("EOS_7_6" ,  "EOS6A14",  6, 7, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1088
  ChInfo("EOS_8_6" ,  "EOS6A16",  6, 8, "EOS", 2, 3, 48, 3, 48, 12, 1, 1, 2, 3986, 0),   # 1089
  ChInfo("EOS_1_-6",  "EOS6C02", -6, 1, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1090
  ChInfo("EOS_2_-6",  "EOS6C04", -6, 2, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1091
  ChInfo("EOS_3_-6",  "EOS6C06", -6, 3, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1092
  ChInfo("EOS_4_-6",  "EOS6C08", -6, 4, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1093 
  ChInfo("EOS_5_-6",  "EOS6C10", -6, 5, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1094
  ChInfo("EOS_6_-6",  "EOS6C12", -6, 6, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1095
  ChInfo("EOS_7_-6",  "EOS6C14", -6, 7, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1096
  ChInfo("EOS_8_-6",  "EOS6C16", -6, 8, "EOS", 2, 3, 48, 3, 48, 12, 2, 2, 1, 3986, 0),   # 1097
]                                                                                          
MAXCHAMBERS = len(mdt) 

  # BMS cutout BOS6-projective, BOG-rail, EMS1/3, !!!                                      

smdt_chambers = ('BME4A13','BME4C13','BMG2A12','BMG2A14','BMG2C12','BMG2C14','BMG4A12','BMG4A14','BMG4C12','BMG4C14','BMG6A12','BMG6A14','BMG6C12','BMG6C14','BIS7A02','BIS7A04','BIS7A06','BIS7A08','BIS7A10','BIS7A12','BIS7A14','BIS7A16')

# List of chambers with cutouts
cutout_chambers = ( \
'BIR1A11', 'BIR1A15', 'BIR1C11', 'BIR1C15',
'BIR2A11', 'BIR2A15', 'BIR2C11', 'BIR2C15',
'BIR4A11', 'BIR4A15', 'BIR4C11', 'BIR4C15',
'BIR5A11', 'BIR5A15', 'BIR5C11', 'BIR5C15',
'BMS4A02', 'BMS4A04', 'BMS4A06', 'BMS4A08', 'BMS4A10', 'BMS4A16', \
'BMS4C02', 'BMS4C04', 'BMS4C06', 'BMS4C08', 'BMS4C10', 'BMS4C16', \
'BMS6A02', 'BMS6A04', 'BMS6A06', 'BMS6A08', 'BMS6A10', 'BMS6A16', \
'BMS6C02', 'BMS6C04', 'BMS6C06', 'BMS6C08', 'BMS6C10', 'BMS6C16', \
'BMG2A12', 'BMG2A14', 'BMG4A12', 'BMG4A14', 'BMG6A12', 'BMG6A14', \
'BMG4C12', 'BMG4C14', 'BMG2C12', 'BMG2C14', 'BMG6C12', 'BMG6C14')

# Channel Mapping - not sure these are correct
# for all 3x8 chambers
mapI  = [6, 1, 8, 2, 7, 3, 5, 4] # HH Type 1	map3x8b
mapII = [8, 1, 7, 2, 6, 3, 5, 4] # HH Type 2	map3x8a

# for 4x6 EndCap chambers (EIS,EIL in H8)
mapIII = [5, 6, 4, 2, 3, 1] # Type 3    map436
mapIV  = [4, 2, 5, 1, 6, 3] # Type 4    map446
map4x6 = [4, 1, 5, 2, 6, 3] # special   map4x6

map4x6g = [2, 1, 3, 5, 4, 6] # Type 5 BIM #1,3 on EIL4 for III
map4x6h = [3, 5, 2, 6, 1, 4] # Type 6 BIM #0,2 on EIL4 for IV
# for 4x6 barrel chambers (BIL1,BIL2 in H8)
map4x6d = [1, 3, 2, 4, 6, 5] # 
map4x6e = [3, 6, 1, 5, 2, 4] # 
map4x6f = [3, 6, 2, 5, 1, 4] # 

#  mezzchannelmap is a map of mezzcard channels for all 4 mezzcard types.
#  It maps mezzcard type,layer,tube to mezzcard channel as follows:
#  mezzcard channel = mezzchannelmap[type][layer][tube]  
#  where type=1..4, layer=1..4, tube=1..8
#  -1's are used for the 0 indices so that the type,layer, tube numbering 
#  starts at 1 rather than 0 to match the hardware numbering conventions.
#  Also -1's are inserted to make all tube indices go to 8, to prevent 
#  accidental array out of bounds.
#  However, -1 indicates that the type,layer,tube combination is invalid
#  Implement as tuple for immutability and speed. 
#  There are 7 maps (index 0 being a placeholder, so that maps are valid for mezzcard types 1..6

# ORIGINAL
mezzchannelmap = ( 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ), 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,0,4,2),         (-1,9,11,13,15,14,8,12,10),   (-1,17,19,21,23,22,16,20,18), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,4,2,0),         (-1,9,11,13,15,14,12,10,8),   (-1,17,19,21,23,22,20,18,16), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,5,3,4,2,0,1,-1,-1),       (-1,11,9,10,8,6,7,-1,-1),     (-1,17,15,16,14,12,13,-1,-1), (-1,23,21,22,20,18,19,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,3,1,5,0,2,4,-1,-1),       (-1,9,7,11,6,8,10,-1,-1),     (-1,15,13,17,12,14,16,-1,-1), (-1,19,21,23,18,20,22,-1,-1) ) 
)

#  Map for BA side chambers.
mezzchannelmapBA = ( 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ), 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,0,4,2),         (-1,9,11,13,15,14,8,12,10),   (-1,17,19,21,23,22,16,20,18), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,4,2,0),         (-1,9,11,13,15,14,12,10,8),   (-1,17,19,21,23,22,20,18,16), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,0,2,4,3,5,-1,-1),       (-1,7,6,8,10,9,11,-1,-1),     (-1,13,12,14,16,15,17,-1,-1), (-1,19,18,20,22,21,23,-1,-1) ),  #changed
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,4,2,0,5,1,3,-1,-1),       (-1,10,8,6,11,7,9,-1,-1),     (-1,16,14,12,17,13,15,-1,-1), (-1,22,20,18,23,21,19,-1,-1) )   #changed
)
#  Map for BC side chambers.
mezzchannelmapBC = ( 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ), 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,0,4,2),         (-1,9,11,13,15,14,8,12,10),   (-1,17,19,21,23,22,16,20,18), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,4,2,0),         (-1,9,11,13,15,14,12,10,8),   (-1,17,19,21,23,22,20,18,16), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,19,18,20,22,21,23,-1,-1), (-1,13,12,14,16,15,17,-1,-1), (-1,7,6,8,10,9,11,-1,-1),     (-1,1,0,2,4,3,5,-1,-1)       ),  #changed
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,22,20,18,23,21,19,-1,-1), (-1,16,14,12,17,13,15,-1,-1), (-1,10,8,6,11,7,9,-1,-1),     (-1,4,2,0,5,1,3,-1,-1)       )   #changed
)
#BEE1A, 
mezzchannelmapEA = ( 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ), 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,0,4,2),         (-1,9,11,13,15,14,8,12,10),   (-1,17,19,21,23,22,16,20,18), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,4,2,0),         (-1,9,11,13,15,14,12,10,8),   (-1,17,19,21,23,22,20,18,16), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,5,3,4,2,0,1,-1,-1),       (-1,11,9,10,8,6,7,-1,-1),     (-1,17,15,16,14,12,13,-1,-1), (-1,23,21,22,20,18,19,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,3,1,5,0,2,4,-1,-1),       (-1,9,7,11,6,8,10,-1,-1),     (-1,15,13,17,12,14,16,-1,-1), (-1,19,21,23,18,20,22,-1,-1) ) 
)
#  Map for C side chambers.
mezzchannelmapEC = ( 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ), 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,0,4,2),         (-1,9,11,13,15,14,8,12,10),   (-1,17,19,21,23,22,16,20,18), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,4,2,0),         (-1,9,11,13,15,14,12,10,8),   (-1,17,19,21,23,22,20,18,16), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,23,21,22,20,18,19,-1,-1), (-1,17,15,16,14,12,13,-1,-1), (-1,11,9,10,8,6,7,-1,-1),     (-1,5,3,4,2,0,1,-1,-1)       ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,19,21,23,18,20,22,-1,-1), (-1,15,13,17,12,14,16,-1,-1), (-1,9,7,11,6,8,10,-1,-1),     (-1,3,1,5,0,2,4,-1,-1)       ) 
)

mezzchannelmapBIR3 = ( 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ), 
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,0,4,2),         (-1,9,11,13,15,14,8,12,10),   (-1,17,19,21,23,22,16,20,18), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,1,3,5,7,6,4,2,0),         (-1,9,11,13,15,14,12,10,8),   (-1,17,19,21,23,22,20,18,16), (-1,-1,-1,-1,-1,-1,-1,-1,-1) ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,22,21,23,20,18,19,-1,-1), (-1,16,15,17,14,12,13,-1,-1), (-1,10,9,11,8,6,7,-1,-1),     (-1,4,3,5,2,0,1,-1,-1)       ),
    ( (-1,-1,-1,-1,-1,-1,-1,-1,-1), (-1,23,21,19,18,20,22,-1,-1), (-1,17,13,15,12,14,16,-1,-1), (-1,11,7,9,6,8,10,-1,-1),     (-1,5,1,3,0,2,4,-1,-1)       ) 
)


#  Number of tubes per layer in mezzcard
mezzcardtubes = ( -1, 8, 8, 6, 6, 6, 6 )

#  Check for valid chamber name.
#  Returns mdt[] index of chamber, else -1 
def CheckChamber( chamber, ml, layer, tube ): 
  for ii in range(0,MAXCHAMBERS):
    if (chamber == mdt[ii].hardname or chamber == mdt[ii].calname) and \
    layer>0 and tube>0 and \
      ( (ml==1 and layer<=mdt[ii].nly_ml1 and tube<=mdt[ii].ntl_ml1) or \
        (ml==2 and layer<=mdt[ii].nly_ml2 and tube<=mdt[ii].ntl_ml2) ):
      return ii;
  return -1

#  Returns index in mdt[] for ChName
def MDTindex( ChName ):
  if type(ChName) is int:  #assume to be an index or muonfixedid if int
#  If looks like an chamber index already, just return
    if ChName > 0 and ChName < MAXCHAMBERS: return ChName
#  Else treat as muonfixedid
    else: 
      ChName = '%s_%i_%i'%(muonfixedid.stationNameString(ChName),muonfixedid.stationPhi(ChName),muonfixedid.stationEta(ChName)) 
#      return MDTcheckMfid(ChName)
  return CheckChamber( ChName, 1,1,1)

#  Returns muonfixedid for ChName
def MDTmfid( ChName, ml=1, ly=1, tb=1 ):
# if ml > 1000 assume it is an MLLT
  if ml > 1000:
    mllt = ml
    ml,ly,tb = MLLT2MLLT(ChName,mllt)
# Check that ChName, ml,ly,tb are valid 
  idx = CheckChamber(ChName,ml,ly,tb) 
  if idx == -1: return -1
  return muonfixedid.ID(mdt[idx].station,mdt[idx].phi,mdt[idx].eta,ml,ly,tb)

#  Checks muonfixedid: Returns MDTindex if mfid is valid; -1 if not
def MDTcheckMfid( mfid ):
  ChName = '%s_%i_%i'%(muonfixedid.stationNameString(mfid),muonfixedid.stationPhi(mfid),muonfixedid.stationEta(mfid)) 
  ml  = muonfixedid.mdtMultilayer(mfid)
  ly  = muonfixedid.mdtTubeLayer(mfid)
  tb  = muonfixedid.mdtTube(mfid)
# Check that ChName, ml,ly,tb are valid 
  return CheckChamber(ChName,ml,ly,tb)

# Returns hardware name from idx (which can be actual mdt[] index, calname or muonfixedid)
# Cannot overload functions so allow argument to be int (idx) or string (calibname)
def MDThardname( idx ):
  if type(idx) is int:
    if idx<0: return "IsWRONG"
    elif idx>=MAXCHAMBERS:   #assume it is a muonfixedid
      idx = MDTcheckMfid(idx)
  else:       #Assume is a hardname or calname
    idx = MDTindex( idx )
  if idx == -1: return "IsWRONG"
  return mdt[idx].hardname

# Returns calname from idx (which can be actual mdt[] index, hardname or muonfixedid)
def MDTcalname( idx ):
  if type(idx) is int:
    if idx<0: return "IsWRONG"
    elif idx>=MAXCHAMBERS:   #assume it is a muonfixedid
      idx = MDTcheckMfid(idx)
  else:       #Assume is a hardname or calname
    idx = MDTindex( idx )
  if idx == -1: return "IsWRONG"
  return mdt[idx].calname

# Returns hardware name-ML-L-T from a muonfixedid
def MDTtubename( id, chet=0 ):
  idx = MDTindex('%s_%i_%i'%(muonfixedid.stationNameString(id),muonfixedid.stationPhi(id),muonfixedid.stationEta(id)))
  if idx == -1: return 'UnknownMFID %i'%id
  if chet: return '%s-%i-%i-%i'%(mdt[idx].calname,muonfixedid.mdtMultilayer(id),muonfixedid.mdtTubeLayer(id),muonfixedid.mdtTube(id))
  return '%s-%i-%i-%i'%(mdt[idx].hardname,muonfixedid.mdtMultilayer(id),muonfixedid.mdtTubeLayer(id),muonfixedid.mdtTube(id))

# Returns #ML in the chamber
def MDTnML(  ChName ):
  idx = MDTindex( ChName )
  if idx < 0: return -1
  return mdt[idx].num_ml

# Returns TOT #Layers in the chamber
def MDTtotalLayers( ChName ) :
  idx = MDTindex( ChName )
  if idx < 0: return -1
  return MDTnLml( ChName, 0)

# Returns TOT #Tubes  in the chamber
def MDTtotalTubes( ChName ):
  idx = MDTindex( ChName )
  if idx < 0: return -1
  return MDTnTml(ChName,0)

# Returns #Mezzanines in the chamber
def MDTnmezz( ChName ):
  idx = MDTindex( ChName )
  if idx < 0: return -1
  return mdt[idx].n_mezz
  
# Returns number of mezzcards included skipped mezzcards due to cut outs
def MDTmaxmezz( ChName ):
  idx = MDTindex( ChName )
  if idx < 0: return -1
  icutout = 0
  hardname = mdt[idx].hardname
# Only cutouts in BIR and BMS have skipped mezzcards (BMG cutouts don't)
  if hardname[0:3] == 'BIR' or hardname[0:3] == 'BMS':
    for chamber in cutout_chambers:
#  These are the chambers with cutouts
      if hardname == chamber:
        icutout = 1
        break
# BIS2A06 (10 mezzcards) skips mezzcard 9 so max mezzcard is 10
# BIS1A16 (12 mezzcards) skips mezzcard 8 so max mezzcard is 12
  elif hardname == 'BIS2A06' or hardname == 'BIS1A16':
    icutout = 1
  elif hardname[0:5] == "BIS7A":
    icutout= 5

  return mdt[idx].n_mezz + icutout

# Returns mezzcard type on a ML
def MDTtypeML( ChName, ml ):
  if ml!=1 and ml!=2:
    print("MDTtypeML bad ml=%i. Returned 0" % (ml))
    return 0
  idx = MDTindex( ChName )
  if idx < 0: return -1
  mztype = mdt[idx].mzt_ml1
  if ml==2: mztype = mdt[idx].mzt_ml2
  return mztype

# Returns mezzcard type of mezzcard mz
def MDTtypeMZ( ChName, mz ):
  ml = MDTmlMZ( ChName, mz )
  return MDTtypeML( ChName, ml )

# Returns ML which has Mezz#0
def MDTmlMZ0( ChName ):
  idx = MDTindex( ChName )
  if idx < 0: return -1
  return mdt[idx].mz0ml

# Returns #ML of Mezz# mz
def MDTmlMZ( ChName, mz ):
  m0m = MDTmlMZ0(ChName)
  if m0m == -1: return -1
  if mz%2 == 0: return m0m
  return 3-m0m

# Returns #Layer in the ML
def MDTnLml( ChName, ml ):
  if ml<0 or ml>2:
    print("MDTnLml bad ml=%i. Returned 0\n" %(ml))
    return -1
  nl_ml = 0
  idx   = MDTindex( ChName);
  if idx >= 0:
    if   ml==1: nl_ml = mdt[idx].nly_ml1
    elif ml==2: nl_ml = mdt[idx].nly_ml2
    elif ml==0: nl_ml = mdt[idx].nly_ml1 + mdt[idx].nly_ml2
  return nl_ml

# Returns #Tubes/layer in the ML
def MDTnTly( ChName, ml ):
  if ml<0 or ml>2:
    print("MDTnTly bad ml=%i. Returned -1" % (ml))
    return -1
  nt_ly = 0
  idx   = MDTindex( ChName )
  if idx >= 0:
    if   ml==1: nt_ly = mdt[idx].ntl_ml1
    elif ml==2: nt_ly = mdt[idx].ntl_ml2
    elif ml==0: nt_ly = mdt[idx].ntl_ml1 + mdt[idx].ntl_ml2
  return nt_ly

# Returns #Tubes in the ML
def MDTnTml( ChName, ml ):
  if ml<0 or ml>2:
    print("MDTnTml bad ml=%i. Returned -1\n" %(ml))
    return -1
  nt_ml = 0
  idx   = MDTindex( ChName)
  if idx >= 0:
    if   ml==1: nt_ml = mdt[idx].ntl_ml1*mdt[idx].nly_ml1
    elif ml==2: nt_ml = mdt[idx].ntl_ml2*mdt[idx].nly_ml2
    elif ml==0: nt_ml = mdt[idx].ntl_ml1*mdt[idx].nly_ml1 + \
      		  mdt[idx].ntl_ml2*mdt[idx].nly_ml2
  return nt_ml

# Convert MLLT to ml, ly, tb; Returns tuple ml, ly, tb
# Allow 2 formats for MLLT:
# 1.  1000*ml +  100*ly + tb
# 2. 10000*ml + 1000*ly + tb   (needed for BIS7A MDT which have 108 tubes/layer) 
def MLLT2MLLT( ChName, mllt ):
  OK=1 
  if mllt < 10000:    #Most chambers
    ml = mllt/1000
    ly =(mllt/100)%10
    nt = mllt%100
  else:               #BIS7 sMDT
    ml = mllt/10000
    ly =(mllt/1000)%10
    nt = mllt%1000
  if   mllt<=0 or (mllt<10000 and mllt>2478) or mllt>24108: OK=0
  elif ml<1 or ml>2:                  OK=0
  elif ly<1 or ly>MDTnLml(ChName,ml): OK=0
  elif nt<1 or nt>MDTnTly(ChName,ml): OK=0
  if OK == 0:
    ml = -1
    ly = -1
    nt = -1
  return ml, ly, nt 

# Convert tubenum [1..] to MLLT  
def T2MLLT( ChName, tb ):
  idx = MDTindex( ChName )
  if idx<0 or tb<0 or tb>MDTtotalTubes(ChName):
    print(' T2MLLT %s tb=%i tube number out of range'%(ChName,tb))
    return 0

  ml = 1
  if tb > MDTnTml(ChName,1): 
    ml = 2
    tb -= MDTnTml(ChName,1)   #now tubenum in ML2

  ly = 1 + tb/MDTnTly(ChName,ml)
  if tb%MDTnTly(ChName,ml) == 0: ly -= 1

  nt = tb - (ly-1)*MDTnTly(ChName,ml) #now tubenum in that layer
  mllt = ml*1000 + ly*100 + nt
  if nt > 100: mllt = ml*10000 + ly*1000 + nt
# Test if valid mllt
  xml,xly,xnt = MLLT2MLLT( ChName, mllt );
  if xml == -1: mllt=0;
  return mllt;

# Convert MLLT to tubenum
def MLLT2T( ChName, mllt ):
  ml, ly, nt = MLLT2MLLT(ChName, mllt)
  if ml == -1: return -1 
  return (ml-1)*MDTnTml(ChName,1) + (ly-1)*MDTnTly(ChName,ml) + nt

#MLLT to layer [1..] note this is total number of layers, not layer within ML 
def MLLT2L( ChName, mllt ):
  ml, ly, nt = MLLT2MLLT(ChName, mllt)
  if ml == -1: return -1 
  return (ml-1)*MDTnLml(ChName,1) + ly

# MLLT  to mezzNumber
def MLLT2M( ChName,  mllt ):
  imezz = -1
  ml, ly, nt = MLLT2MLLT(ChName, mllt )
  if ml != -1:
# BMGxA12 and BMGxC14 mezzcards installed in opposite order to all other chambers.
# Normally mezzcard number increases with tube number, but in these chambers its the opposite.
    if ChName == 'BMG2A12' or ChName == 'BMG4A12' or ChName == 'BMG6A12' or ChName == 'BMG2C14' or ChName == 'BMG4C14' or ChName == 'BMG6C14':
      imezz = 2*((60-nt)/6) - 1
      if ml == 2: imezz = imezz - 1
    else:
      ntl_mez = 24/MDTnLml(ChName,ml)
      imezz = MDTnML(ChName)*((nt-1)/ntl_mez)
      if ml != MDTmlMZ0(ChName): imezz = imezz + 1
  return imezz 

# Tube number  to mezzNumber
def T2M( ChName,  tb ):
  mllt = T2MLLT( ChName, tb )
  return MLLT2M( ChName,  mllt )

#  Find mezzcard type, mezzcard channel for chamber, mllt combination.
#  cham = Hardware/software name, mdt[] index, or muonfixedid.
#  If muonfixedid is used, it is use to determine the mllt.
#  returns a ntuple of (mezzcard type, mezzcard channel)
#  returns (-1,-1) if error occurs.
#  Note that mezzcard types are 1..4
#            mezzcard channels are 0..23
def MLLT2MezzCh( cham, mllt=0 ):
  mfid = 0
# Code taken from MDThardname
  if type(cham) is int:
    if cham<0: 
      print("MLLT2MezzCh: bad cham",cham)
      return -1,-1
    elif cham>=MAXCHAMBERS:   #assume it is a muonfixedid
      mfid = cham
      idx = MDTindex('%s_%i_%i'%(muonfixedid.stationNameString(mfid),muonfixedid.stationPhi(mfid),muonfixedid.stationEta(mfid))) 
    else:                     #assume is already a chamber index
      idx = cham
  else:       #Assume is a hardname or calname
    idx = MDTindex( cham )

#  Check for invalid chamber
  if idx == -1:
    print('MLLT2MezzCh ERROR: Bad chamber',cham)
    return -1,-1

  ChName = mdt[idx].hardname

#  if muonfixedid has been passed get ml,ly,tb from muonfixedid
#  otherwise get it from mllt
  if mfid == 0:
    (ml,ly,tb) = MLLT2MLLT( ChName, mllt )
  else:
    ml = muonfixedid.mdtMultilayer(mfid)
    ly = muonfixedid.mdtTubeLayer(mfid)
    tb = muonfixedid.mdtTube(mfid)

# Check for valid ml,ly,tb
  if ml<1 or ml>2 \
  or ly<1 or ly>MDTnLml(ChName,ml) \
  or tb<1 or tb>MDTnTly(ChName,ml):
# This print just finds the odd chambers like BMS4, BMS6, BIR 
# which have different number of tubes on each ML
# Database contains these fantom tubes
#    print('MLLT2MezzCh ERROR: Bad chamber or mllt',ChName,mllt)
    return -1,-1
# Get mezzcard type.  sMDT have types 5,6 but are not handled here, use mezzcardmap.py instead.
  mezztype = MDTtypeML( ChName, ml)
  if mezztype==5 or mezztype==6:
#    print('MLLT2MezzCh ERROR: for sMDDT use mezzcardmap.py')
    return mezztype,-1
  elif mezztype < 1 or mezztype > 4:
#    print('MLLT2MezzCh ERROR: Bad mezztype',ChName,mllt,mezztype)
    return -1,-1
    
# tbi = tube index within a mezzcard
  tbi = tb%mezzcardtubes[mezztype]
  if tbi == 0: tbi = mezzcardtubes[mezztype]

  mezzchan = -2
# A/C sides are mirror images, use different maps
  station = ChName[0:3]
  LS      = ChName[2]   # Could be L,S,E,M,R,F,G
  side    = ChName[4]
  if ChName[0] == 'E' or ChName[1] == 'E':   #Endcap + BEE
    if side == 'A': mezzchan = mezzchannelmapEA[mezztype][ly][tbi] 
    else:           mezzchan = mezzchannelmapEC[mezztype][ly][tbi] 
  else:    #Barrel
# BIM1A11 uses C map     BIMxA11 use C map
    if station == 'BIM' or station == 'BIR':
#      if station == 'BIR' and int(ChName[3]) > 2:
#        if side == 'A': mezzchan = mezzchannelmapBIR3[mezztype][ly][tbi] 
#        else:           mezzchan = mezzchannelmapEA[mezztype][ly][tbi] 

      if ChName[5:7] == '11':    #BIMxx11, BIRxx11
        if side == 'A': mezzchan = mezzchannelmapBC[mezztype][ly][tbi] 
        else:           mezzchan = mezzchannelmapBA[mezztype][ly][tbi] 
      elif side == 'A': mezzchan = mezzchannelmapBA[mezztype][ly][tbi] 
      else:             mezzchan = mezzchannelmapBC[mezztype][ly][tbi] 

# BISxx12 
    elif station == 'BIS' and ChName[5:7] == '12': 
      if side == 'A': mezzchan = mezzchannelmapEC[mezztype][ly][tbi] 
      else:           mezzchan = mezzchannelmapEA[mezztype][ly][tbi] 
    elif LS == 'S': 
      if side == 'A': mezzchan = mezzchannelmapEA[mezztype][ly][tbi] 
      else:           mezzchan = mezzchannelmapEC[mezztype][ly][tbi] 
    else:
      if side == 'A': mezzchan = mezzchannelmapBA[mezztype][ly][tbi] 
      else:           mezzchan = mezzchannelmapBC[mezztype][ly][tbi] 
  return mezztype,mezzchan

########################################################################
#  Convert date in DD-<Month>-YYYY to YYYYMMDD
#  where <Month> = Jan, Feb, etc
########################################################################  
def getdate(date):
  date2='0'
#  If do not find date give up
  if date == '':   # return ''
    print('getdate ERROR: no date found for',headID)
    return '0 0'
  
# convert date to YYYYMMDD
  months=["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
  for imon in range(1,12):
    if date[3:6] == months[imon]:
      date2 = date[7:] + '%2.2i'%imon + date[:2]
      break

#  Return date and run number    
  return date2

########################################################################
### Return an idx list of idx numbers for sMDT chambers
########################################################################
def sMDTlist():
  smdtlist = []
  for chamber in smdt_chambers:
    smdtlist.append( MDTindex(chamber) )
  return smdtlist

########################################################################
### Returns max tube length in chamber [mm]
########################################################################
def MDTtubelength( ChName ):
  idx = MDTindex( ChName )
  return mdt[idx].max_tube_len

########################################################################
### This is for testing porpoises
########################################################################
def main(argv):
  cham=''
  mllt=0
  try:
    cham = int(argv[0])
  except:
    cham = argv[0]
    mllt = int(argv[1])
  (mezztype,mezzchan) = MLLT2MezzCh( cham, mllt )
  print('Results',cham,mllt,mezztype,mezzchan)
  return
  print(mdt[0].calname,mdt[1].hardname)
  print("BEE1A04",MDTindex("BEE1A04"))
  print("EOS6C14",MDTindex("EOS6C14"))
  print("EOS6C14-1-1-1",CheckChamber("EOS6C14",1,1,1))
  print("EOS6C14-1-1-89",CheckChamber("EOS6C14",1,1,89))
  chamber="EEL1C13"
  mllt=1208
  for i in range(0,8):
    print(chamber,mllt,MLLT2MLLT( chamber, mllt ),MLLT2M( chamber, mllt ))
    mllt = mllt + 8
  print(MDThardname( 34 ), MDThardname( "BIL_1_1" ))

if __name__ == "__main__":
  main(sys.argv[1:])
