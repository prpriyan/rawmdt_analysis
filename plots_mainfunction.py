#THIS CODE SNIPPET IS FOR CALLING ALL THE DIFFERENT FUNCTIONS -> Ran successfully
import trk_Hist_functions as thf
import pandas as pd
import numpy as np
from array import array
from scipy import stats
import ROOT

def plots(df, driftTube_type, output_filename,chamber):
    canvas = ROOT.TCanvas("canvas", chamber, 2000, 2000)  #hisstogram dim: landscape(length, height)

    pads = []
    for i in range(4):  # Rows
        for j in range(3):  # Columns
            pad_name = f"pad{i*2 + j + 1}"
            xlow = j * 0.33  # Divide width by 4 (25% each)
            xup = (j + 1) * 0.33
            ylow = 1 - (i + 1) * 0.25  # Divide height by 3 (33% each)
            yup = 1 - i * 0.25
                #  ROOT.TPad("name", "title", xlow, ylow, xup, yup)
            pad = ROOT.TPad(pad_name, f"Pad {i*2 + j + 1}", xlow, ylow, xup, yup)
            pad.Draw()
            pads.append(pad)
            
   # HIST 1: DRIFT_TIME SPECTRUM
    pads[0].cd()
    driftTime,leadingEdge_fit,TrailingEdge_fit=thf.driftTime_spectrum(df,driftTube_type)
    driftTime.Draw()
    TrailingEdge_fit.Draw("L same")
    leadingEdge_fit.Draw("L same")

    # Retrieve the fit parameters
    entries=driftTime.GetEntries()
    p0 = leadingEdge_fit.GetParameter(0)  # 
    p1 = leadingEdge_fit.GetParameter(1)  # 
    # p2 = leadingEdge_fit.GetParameter(2)  # offset-> coming out to be 0-> removed
    p3 = leadingEdge_fit.GetParameter(3)

    # Create a TPaveText to display the fit parameters on the canvas
    pave1 = ROOT.TPaveText(0.6, 0.7, 0.9, 0.9, "NDC")  # NDC coordinates (0-1)
    pave1.SetFillColor(0)  # Transparent background
    pave1.SetTextAlign(12)  # Align text to left-center
    pave1.SetTextColor(ROOT.kRed)
    pave1.AddText(f"Entries: {entries}")
    pave1.AddText(f"t0: {p0:.2f} ns")
    pave1.AddText(f"Steepness T: {p1:.2f} ns")
    # pave1.AddText(f"Background Offset: {p2:.2f} ns")
    # pave1.AddText(f"Amplitude: {p3:.2f}")
    pave1.Draw()

    # Retrieve the fit parameters
    q0 = TrailingEdge_fit.GetParameter(0)  # 
    q1 = TrailingEdge_fit.GetParameter(1)  # 
    # q2 = TrailingEdge_fit.GetParameter(2)  # offset-> coming out to be 0-> removed
    q3 = TrailingEdge_fit.GetParameter(3)

    # Create a TPaveText to display the fit parameters on the canvas
    pave2 = ROOT.TPaveText(0.6, 0.5, 0.9, 0.7, "NDC")  # NDC coordinates (0-1)
    pave2.SetFillColor(0)  # Transparent background
    pave2.SetTextAlign(12)  # Align text to left-center
    pave2.SetTextColor(ROOT.kBlue)
    pave2.AddText(f"t_max: {q0:.2f}")
    pave2.AddText(f"Steepness T: {q1:.2f}")
    # pave2.AddText(f"Background Offset: {q2:.2f}")
    pave2.AddText(f"DTmax: {p3-q3:.2f} ns")
    pave2.Draw()

# #HIST 2: ADC COUNT
    pads[1].cd()
    adcCount=thf.ADCcount(df,driftTube_type)
    adcCount.Draw()

#HIST 3: 1-D RESIDUAL WITH GAUSSIAN FIT
    pads[2].cd()
    residualCount, doubleGFit=thf.residual_fit(df)
    residualCount.Draw("COLZ")
    doubleGFit.Draw("P same")

    p0=doubleGFit.GetParameter(0) #peak1
    p1=doubleGFit.GetParameter(1) #sigma1(std deviation1)
    p2=doubleGFit.GetParameter(2)  #mean
    p3=doubleGFit.GetParameter(3)  #peak2
    p4=doubleGFit.GetParameter(4)  #sigma2(std deviation2)

    pave = ROOT.TPaveText(0.6, 0.7, 0.9, 0.9, "NDC")  # NDC coordinates (0-1)
    pave.SetFillColor(0)  # Transparent background
    pave.SetTextAlign(12)  # Align text to left-center
    pave.SetTextColor(ROOT.kRed)
    pave.AddText(f"Entries: {entries}")
    pave.AddText(f"Peak1: {p0:.2f}")
    pave.AddText(f"Std_dev1: {p1:.2f} um")
    pave.AddText(f"Mean: {p2:.2f} um")
    pave.AddText(f"Peak2: {p3:.2f}")
    pave.AddText(f"Std_dev2: {p4:.2f} um")
    pave.Draw()


#HIST 4: DRIFT_RADIUS COUNT
    pads[3].cd()
    drift_RadiusCount=thf.driftRadius_dist(df)
    drift_RadiusCount.Draw()

#HIST5:TrkHitmap
    pads[4].cd()
    trkhitmap=thf.Hitmap(df,chamber)#"BOL4A13"
    trkhitmap.Draw()

#HIST 6:  Residual vs DistRO
    pads[5].cd()
    ResVsDistRO=thf.ResVsDistRO(df,driftTube_type)
    ResVsDistRO.Draw("COLZ")

#HIST 7: RESIDUAL VS 1/ADC
    pads[6].cd()
    residualVsADC=thf.ResVsInverseAdc(df)
    residualVsADC.Draw("COLZ")

# HIST 8: RESIDUAL VS HIT_RADIUS
    pads[7].cd()
    resVsHitRadius,profile_x=thf.ResVsRadius(df)
    resVsHitRadius.Draw("COLZ")
    profile_x.Draw("SAME") # Draw on the same canvas


#HIST 9: ADC VS DISTRO
    pads[8].cd()
    adcVsDistRO=thf.AdcVsDistRO(df,driftTube_type)
    adcVsDistRO.Draw("COLZ")

#HIST 10: RADIUS VS DRIFT_TIME
    pads[9].cd()
    radVsdriftTime=thf.RadiusVsDriftTime(df,driftTube_type)
    radVsdriftTime.Draw("COLZ")
    
    canvas.Update()
    return canvas.SaveAs(output_filename)
# plots(df_trk_MDT,"MDT","P_MDT.pdf","BOL4A13")

