import uproot
import ROOT  #do no need uproot if using pyroot, can just use it by importing ROOT and using python binding of 
# C++ classes in python
import numpy as np
import matplotlib.pyplot as plt
import awkward as ak
import PyPDF2

fileON=uproot.open("user.rdinardo.41492072.EXT0._000001.calib_NSWon.root")
fileOFF=uproot.open("user.rdinardo.41491131.EXT0._000001.calib_NSWoff.root")

def create_df(inputfile):
    file=inputfile   
    tree=file['Segments']    #-> tells how many branches are in this ttree

    branch_names=tree.keys()
    branchPrefix=['trk', 'trkHit', 'event', 'rawMdt','rawRPC','rawTgc','trkTriggerHit']      
    branchMapping={key : [] for key in branchPrefix} 

    for branch in branch_names:          #separate out the prefix in keys name and append it to the branchMapping dictionary. why??
        branchName, variableName= branch.split('_', 1) 
        if branchName in branchPrefix:
            branchMapping[branchName].append(branch)  

    df_trk=ak.to_dataframe(tree.arrays(branchMapping['trkHit']))
    return df_trk

output_pdf_list=[]
#Function to draw plots on histogram
from ROOT import TCanvas, TPad, TGraph

def plot_hist(hist_titleON,hist_titleOFF, dataON, dataOFF, n_bins,v_rangeON,v_rangeOFF, x_labelON, x_labelOFF, outputFile):
    canvas=TCanvas("canvas", "Two pads for comparison",800,600) #
    # pad1=TPad("pad1",'Pad1',0.0,0.5,1.0,1.0)  #
    # pad2=TPad("pad1",'Pad1',0.0,0.0,1.0,0.5)
    # pad1.Draw()
    # pad2.Draw()

    # pad1.cd()   #change to pad1
    histON=ROOT.TH1F("hist_ON", hist_titleON ,n_bins,v_rangeON[0],v_rangeON[1] )  #no of bins, -4,4: range

    for value in dataON:
        histON.Fill(value)
    
    histON.GetXaxis().SetTitle(x_labelON)
    histON.GetYaxis().SetTitle("Counts")
    histON.SetLineColor(ROOT.kBlue)  # Set color for histogram ON
    histON.SetStats(0)



    histON.Draw()

    # pad2.cd()  #change to pad2
    histOFF=ROOT.TH1F("hist_OFF", hist_titleOFF, n_bins,v_rangeOFF[0],v_rangeOFF[1] )  #hist_name, hist_title, n_bins, range

    for value in dataOFF:
        histOFF.Fill(value)

    histOFF.GetXaxis().SetTitle(x_labelOFF)
    histOFF.GetYaxis().SetTitle("Counts")
    histOFF.SetLineColor(ROOT.kRed)   # Set color for histogram OFF
    histOFF.SetStats(0)

    histOFF.Draw("SAME")

    # Create and display statistics for the first histogram
    # stats1 = ROOT.TPaveStats(x1, y1, x2, y2)  #bottom-left. upper right
    stats1 = ROOT.TPaveStats(0.7, 0.7, 0.9, 0.9, "NDC") #, "NDC"> normalised to the canvas size
    stats1.SetBorderSize(1)
    stats1.SetFillColor(ROOT.kWhite)
    stats1.SetTextColor(ROOT.kBlue)
    stats1.SetTextSize(0.02)
    stats1.AddText(f"NSWON/ Entries: {histON.GetEntries()}")
    stats1.AddText(f"Mean: {histON.GetMean():.2f}")
    stats1.AddText(f"RMS: {histON.GetRMS():.2f}")
    
    stats1.Draw()

    # Create and display statistics for the second histogram
    stats2 = ROOT.TPaveStats(0.7, 0.5, 0.9, 0.7, "NDC")
    stats2.SetBorderSize(1)
    stats2.SetFillColor(ROOT.kWhite)
    stats2.SetTextColor(ROOT.kRed)
    stats1.SetTextSize(0.02)
    stats2.AddText(f"NSWOFF/ Entries: {histOFF.GetEntries()}")
    stats2.AddText(f"Mean: {histOFF.GetMean():.2f}")
    stats2.AddText(f"RMS: {histOFF.GetRMS():.2f}")
    
    stats2.Draw()

    canvas.Update()
    canvas.SaveAs(outputFile)
    return (canvas,outputFile)


df_trkON=create_df(fileON)
df_trkOFF=create_df(fileOFF)
branch_lst=df_trkON.columns
# n_bins=50

for name in branch_lst:

    dataON=df_trkON[name]
    dataOFF=df_trkOFF[name]
    v_rangeON=(dataON.min(), dataON.max())
    v_rangeOFF=(dataOFF.min(), dataOFF.max())
    if name=='trkHit_adc':
        n_bins=300
    # plot_hist(hist_titleON, hist_titleOFF, dataON, dataOFF, n_bins,v_rangeON,v_rangeOFF, x_labelON, x_labelOFF, outputFile):
        Canvas, output=plot_hist(name +"_ON", name+"_OFF" , dataON, dataOFF, n_bins, (0,300),(0,300), name.split('_')[1] + "_ON" , name.split('_')[1] +"_OFF" , f"compare_ON_OFF_{name}.pdf" )

    elif name in ['trkHit_bkgTime', 'trkHit_centerX','trkHit_centerY', 'trkHit_trackIndex','trkHit_triggerTime','trkHit_type']:
        n_bins=7
        Canvas, output=plot_hist(name +"_ON", name+"_OFF" , dataON, dataOFF, n_bins, v_rangeON, v_rangeOFF, name.split('_')[1] + "_ON" , name.split('_')[1] +"_OFF" , f"compare_ON_OFF_{name}.pdf" )

    else:
        n_bins=50
        Canvas, output=plot_hist(name +"_ON", name+"_OFF" , dataON, dataOFF, n_bins, v_rangeON, v_rangeOFF, name.split('_')[1] + "_ON" , name.split('_')[1] +"_OFF" , f"compare_ON_OFF_{name}.pdf" )

    output_pdf_list.append(output)


def combine_pdf(pdf_list, output_pdf):
    pdf_writer=PyPDF2.PdfWriter()

    for pdf in output_pdf_list:
        with open(pdf, 'rb') as pdf_file:
            pdf_reader=PyPDF2.PdfReader(pdf_file)

            for page in range(len(pdf_reader.pages)):
                pdf_writer.add_page(pdf_reader.pages[page])
        
    with open(output_pdf, 'wb') as output_file:
        pdf_writer.write(output_file)
    

output_file="NSW_ON_OFF.pdf"
combine_pdf(output_pdf_list, output_file)

"""
    # n_bins= 250-> adc
    # bkgTime-> maybe reduce bins
    # calibStatus -> definitely reduce bins to 10/20
    # centerX-> optional
    # center Y -> fine
    sagTime -> can reduce
    trackIndex-> reduce to 7 bins
    triggerTime-> reduce to 5 bins maybe
    type-> reduce to 6-7 bins

"""
