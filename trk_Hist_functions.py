#WRITING SEPARATE FUNCTIONS FOR EACH HISTOGRAM-> THEN PU IN THE CONDITIONALS FOR SMDT AND MDT
import ROOT
import numpy as np
from scipy import stats

# HIST 1: DRIFT_TIME SPECTRUM   
# def mt_t0_fermi(x, par):
    
#         t = x[0]      # Independent variable (x-axis value)-> independent var t is referenced as x[0] in pyroot.
#         t_0 = par[0]  # Midpoint (t0): controls where the function transitions
#         T = par[1]    # Steepness parameter 
#         back = par[2] # Background offset
#         A = par[3]    # Amplitude
#         # Fermi function
        # return back + A / (1 + np.exp(-(t - t_0) / T))

def driftTime_spectrum(df,drift_tube_type):
# DriftTime_values=df['trkHit_driftTime'].values.astype(float) #convert driftTime values to flloat values why? were they string before?->CHE
    if (drift_tube_type=="sMDT"):
        print("entered smdt if condition")
        tlow = -100   #range for histogram
        thigh = 350
        binsize = 1
        t0FitRange = [30, 150]  
        tmaxFitRange = [250, 310]  #change this value for sMDT ie [250,310]=> derived from Zhen's python valye function
        hist_values,bins= np.histogram(df['trkHit_driftTime'].astype(float), bins = np.arange(tlow,thigh,binsize)) # np histogram to extract bin_values; #bin_no:450
        #PyROOT histogram 
        driftTime = ROOT.TH1F("hist_driftTime", "DriftTime Spectrum;driftTime;Entries", 450, -100, 300)
        leading_xmin, leading_xmax = -20, 40  # Define the range for the fitting function
        trailing_xmin, trailing_xmax= 160,210
        leading_fermi_fit = ROOT.TF1("leading_mt_t0_fermi", "[2] + [3] / (1 + exp(-(x[0] - [0]) / [1]))", leading_xmin, leading_xmax, 4) #2nd parameter of function: wrote the mt_t0_fermi function 
        # leading_fermi_fit = ROOT.TF1("leading_mt_t0_fermi", "", leading_xmin, leading_xmax, 1) #2nd parameter of function: wrote the mt_t0_fermi function 

        # as strugn literal and in place of "back", "A", ...wrote [2],[3] from the function defined(commented) above
        trailing_fermi_fit= ROOT.TF1("trailing_mt_t0_fermi", "[2] + [3] / (1 + exp(-(x[0] - [0]) / [1]))" , trailing_xmin, trailing_xmax, 4)

        # Set initial parameters for the fit: (t0, T(steepness), background offset, amplitude)
        leading_fermi_fit.SetParameters(0, 2.0, 0.0, 0.8*np.max(hist_values))
        # leading_fermi_fit.SetParameters(0,600.0) #Midpoint (t0)[0]: controls where the function transitions
        # leading_fermi_fit.SetParameters(1,1.0) #Steepness parameter[1]
        # leading_fermi_fit.SetParameters(2,0.0) # Background offset[2]-> no background offset?
        # leading_fermi_fit.SetParameters(3,np.max(hist_values)) # Amplitude-> maximum amplitude value 

        leading_fermi_fit.SetLineColor(ROOT.kRed)
        leading_fermi_fit.SetLineWidth(1)
        # object.SetParLimits(parameter_index, lower_limit, upper_limit)
        # leading_fermi_fit.SetParLimits(2, 0.0, 100.0)  # Limit the 'back' parameter (par[2]) to positive values


        trailing_fermi_fit.SetParameters(180.0, -3.0, 0.0, 0.4*(np.max(hist_values)))
        # trailing_fermi_fit.SetParameters(0,180) #Midpoint (t0): controls where the function transitions
        # trailing_fermi_fit.SetParameters(1,-1.0)
        # trailing_fermi_fit.SetParameters(2, 0.0)
        # trailing_fermi_fit.SetParameters(3,500)
        trailing_fermi_fit.SetLineColor(ROOT.kBlue)
        trailing_fermi_fit.SetLineWidth(1)
        # trailing_fermi_fit.SetParLimits(2, 0.0, 300.0)

    elif (drift_tube_type=='MDT'):
        print("entered mdt if condition")
        tlow = -100 #range for histogram
        thigh =1000
        binsize = 1
        t0FitRange = [30, 150] 
        tmaxFitRange = [750, 890]  #change this value for sMDT ie [250,310]
        hist_values,bins= np.histogram(df['trkHit_driftTime'].astype(float), bins = np.arange(tlow,thigh,binsize)) # np histogram to extract bin_values; #bin_no:450
        #PyROOT histogram 
        driftTime = ROOT.TH1F("hist_driftTime", "DriftTime Spectrum;driftTime;Entries", 900, -100,1000)

        #FITTING
        leading_xmin, leading_xmax = -20, 70  # Define the range for the fitting function
        trailing_xmin, trailing_xmax= 600,820
        leading_fermi_fit = ROOT.TF1("leading_mt_t0_fermi", "[2] + [3] / (1 + exp(-(x[0] - [0]) / [1]))", leading_xmin, leading_xmax, 4)
        trailing_fermi_fit= ROOT.TF1("trailing_mt_t0_fermi", "[2] + [3] / (1 + exp(-(x[0] - [0]) / [1]))", trailing_xmin, trailing_xmax, 4)

        # Set initial parameters for the fit: (t0, T, background, amplitude)
        leading_fermi_fit.SetParameters(1.0, 1.0, 0.0, 110.0)#amplitude changed to 98 from 100:not much impact
        trailing_fermi_fit.SetParameters(720.0, -1.0, 0.0, 20.0)

        leading_fermi_fit.SetLineColor(ROOT.kRed)
        leading_fermi_fit.SetLineWidth(1)
        leading_fermi_fit.SetParLimits(2, 0.0, 200.0)  # Limit the 'back' parameter (par[2]) to positive values
        trailing_fermi_fit.SetLineColor(ROOT.kBlue)
        trailing_fermi_fit.SetLineWidth(1)
        trailing_fermi_fit.SetParLimits(2, 0.0, 50.0)
        # Fit the histogram using the Fermi function

    driftTime.GetXaxis().SetTitle("DriftTime")
    driftTime.GetYaxis().SetTitle("Entries")
    driftTime.SetStats(0)
    # no_of_bins=(abs(tlow)+abs(thigh))/len(hist_values) {trial_version}

    # Iterate over the DataFrame to fill the histogram
    for index, row in df.iterrows():
        drift_Time = row['trkHit_driftTime']
        driftTime.Fill(drift_Time)
    return driftTime,leading_fermi_fit,trailing_fermi_fit

#HIST 2: ADC COUNT
def ADCcount(df,driftTube_type):
    if (driftTube_type=='MDT'):
        adcCount = ROOT.TH1F("hist_adc", "ADC Count;adc;Entries", 75, 50, 370)
    if (driftTube_type=='sMDT'):
    # Create a histogram for ADC counts with 75 bins in the range [50, 350]
        adcCount = ROOT.TH1F("hist_adc", "ADC Count;adc;Entries", 75, 50, 350)
        
    adcCount.GetXaxis().SetTitle("ADC")
    adcCount.GetYaxis().SetTitle("Entries")
    adcCount.SetStats(1)
    
    for index, row in df.iterrows():
        adc = row['trkHit_adc']
        adcCount.Fill(adc)
    return adcCount

#HIST 3: 1-D RESIDUAL DISTRIBUTION WITH GAUSSIAN FIT
def residual_fit(df):
    mdt_resi=df['trkHit_resi'].values.astype(float)
    bin_values,bins = np.histogram(mdt_resi,bins = 200, range=(-1,1))  #no of bins=200; 2000/10 from mdt_Calibfunctions


    # calculate mean and std for histogram data
    bin_center=(bins[:-1]+bins[1:])/2
    mean = np.average(bin_center,weights=bin_values)
    std = np.sqrt(np.average((bin_center - mean)**2, weights=bin_values))

# Reference for doouble gausian fit formula
    # def doubleG_fit(x, peak1, sigma1,mean,peak2,sigma2):
    #     t = (x-mean)/sigma1
    #     narrow = peak1*np.exp(-0.5*t*t)
    #     t = (x-mean)/sigma2
    #     wide = peak2*np.exp(-0.5*t*t)
    #     return narrow + wide
#peak1:[0] ; sigma1:[1] ; mean:[2] ; peak2:[3] ; sigma2:[4]
# write narrow as string:   [0]*exp(-0.5*((x-[2])/[1])*((x-[2])/[1])) + [3]*exp(-0.5*((x-[2])/[4])*((x-[2])/[4]))

    doubleGFit=ROOT.TF1("Double_Gaussian_Fit","[0]*exp(-0.5*(((x-[2])/[1])**2)) + [3]*exp(-0.5*(((x-[2])/[4])**2))",-0.6,0.6)
    doubleGFit.SetParameters(max(bin_values),std/2,mean,max(bin_values)/10,std*2) #max bin_values/10
    doubleGFit.SetLineWidth(1)

    residualCount=ROOT.TH1F("residual_Count", "Residual_Distribution; residual;Entries",200,-1,1) #Range: -1-1 for residual dist
    residualCount.GetXaxis().SetTitle("Residual(um)")
    residualCount.GetYaxis().SetTitle("Entries")
    residualCount.SetStats(0)

    for index, row in df.iterrows():
        residual = row['trkHit_resi']
        residualCount.Fill(residual)

    residualCount.Fit(doubleGFit,"R")
    return residualCount, doubleGFit

#HIST 4: DRIFT_RADIUS COUNT
def driftRadius_dist(df):
    driftRadiusCount=ROOT.TH1F("drift_radius_count", "DriftRadius Distribution;driftRadiusCount;Entries", 150, -10, 10)
    driftRadiusCount.GetXaxis().SetTitle("DriftRadius")
    driftRadiusCount.GetYaxis().SetTitle("Entries")
    driftRadiusCount.SetStats(1)

    for index, row in df.iterrows():
        driftRadius = row['trkHit_driftRadius']
        driftRadiusCount.Fill(driftRadius)
    driftRadiusCount.SetFillColor(ROOT.kBlue)
    driftRadiusCount.SetFillStyle(1001)
    return driftRadiusCount

#HIST 5: TRKHIT_HITMAP
def Hitmap(df,chamber):
    chamberinfo =df['trkHit_tubeName'].tolist()
    ml = [int(x.split('-')[1]) for x in chamberinfo] #?? FIGURE OUT THE TRK HITMAP
    ly = [int(x.split('-')[2]) for x in chamberinfo]
    nLayer = [(int(x.split('-')[1]) - 1) * 4 + int(x.split('-')[2]) for x in chamberinfo]
    tb = [int(x.split('-')[3]) for x in chamberinfo]

    x_bins = 108
    y_bins = 8 

    x_range = [0, 108] # 108 chambers in sMDT
    y_range = [1,9]   #no of layers in 2 multilayers:8
    hitmap = ROOT.TH2F("hitmap", f"{chamber}_hitMap_trkHit;Tube;Layer", x_bins, x_range[0], x_range[1],y_bins, y_range[0], y_range[1])
    hitmap.SetStats(False)

    for t, n in zip(tb, nLayer):
        hitmap.Fill(t, n)
# Adding a color scale (palette)
    palette = hitmap.GetListOfFunctions().FindObject("palette")
    if palette:
        palette.SetX1NDC(0.88)  # Adjust the position of the color bar
        palette.SetX2NDC(0.92)
    
    return hitmap

def ResVsDistRO(df,driftTube_type):
    if (driftTube_type=='sMDT'):
        ResVsDistRO =ROOT.TH2D("ResidualVsDistRO", "Residual vs. DistRO", 180, 0, 1800, 180, -1,1)
    
    if (driftTube_type=='MDT'):
        ResVsDistRO =ROOT.TH2D("ResidualVsDistRO", "Residual vs. DistRO", 180, -1, 5000, 180, -1,1)
    
    ResVsDistRO.GetXaxis().SetTitle("DistRO")
    ResVsDistRO.GetYaxis().SetTitle("Residual")
    ResVsDistRO.GetZaxis().SetTitle("Entries")
    ResVsDistRO.SetStats(1) # Enable the statistics box

    for _, row in df.iterrows():  #generally, index, row but we don't need index here
        residual = row['trkHit_resi']  #returns each row value for col 'trkHit_resi'
        distRO=row['trkHit_distRO']
        ResVsDistRO.Fill(distRO,residual)  #fills the 2D histogram 
    return ResVsDistRO

def ResVsInverseAdc(df):
    residualVsADC= ROOT.TH2D("residualVsADC", "Residual vs 1/ADC", 100, 0, 0.02, 100, -9, 9)
    residualVsADC.GetXaxis().SetTitle("1/ADC")
    residualVsADC.GetYaxis().SetTitle("Residual")
    residualVsADC.GetZaxis().SetTitle("Entries")
    residualVsADC.SetStats(1)

    # Iterate over the DataFrame to fill the histogram
    for index, row in df.iterrows():
        residual = row['trkHit_resi']
        adc = row['trkHit_adc']
        # print(residual, 1/adc)
        residualVsADC.Fill( 1/adc, 25 * residual)  #why multiplying with 25
    return residualVsADC

 # HIST 8: RESIDUAL VS HIT_RADIUS
def ResVsRadius(df):
    # statistic, bin_edges, binnumber = binned_statistic(x, values, statistic='mean', bins=10, range=None) -> statistic: mean, std,median,count... 

    bin_means, bin_edges, binnumber = stats.binned_statistic(df['trkHit_resi'], df['trkHit_resi'], statistic='mean', bins=150)
    #bin_means gives mean of each bin

    # hist2 = ROOT.TH2D("hist2", title2, bins1, range1[0], range1[1], bins2, range2[0], range2[1])
    resVsHitRadius =ROOT.TH2D("resVsHitRadius", "Residuals vs. Hit Radius", 150, -8, 8, 150, -1000,1000)

    resVsHitRadius.GetXaxis().SetTitle("Radius")
    resVsHitRadius.GetYaxis().SetTitle("Residual")
    resVsHitRadius.GetZaxis().SetTitle("Entries")
    resVsHitRadius.SetStats(1)

    for index, row in df.iterrows():
        residual = row['trkHit_resi']
        radius = row['trkHit_driftRadius']

        resVsHitRadius.Fill(radius,residual*1000)  

    #ProfileX histogram-> for mean
    profile_x = resVsHitRadius.ProfileX("_pfx")
    profile_x.SetLineColor(ROOT.kRed)  # Set a different color for the profile histogram
    return resVsHitRadius,profile_x

def AdcVsDistRO(df,driftTube_type):
    if (driftTube_type=='sMDT'):
        adcVsDistRO =ROOT.TH2D("adcVsDistRO", "Adc vs. DistRO", 200, 0, 1800,200, 50,350)
    
    if (driftTube_type=='MDT'):
        adcVsDistRO =ROOT.TH2D("adcVsDistRO", "Adc vs. DistROO", 200, 0, 5000,200, 50,350)
    # adcVsDistRO =ROOT.TH2D("adcVsDistRO", "Adc vs. DistRO", 200, 0, 1800,200, 50,350)
    adcVsDistRO.GetXaxis().SetTitle("distRO")
    adcVsDistRO.GetYaxis().SetTitle("adc")
    adcVsDistRO.GetZaxis().SetTitle("Entries")
    adcVsDistRO.SetStats(1)
    # Iterate over the DataFrame to fill the histogram
    for index, row in df.iterrows():
        adc = row['trkHit_adc']
        DistRO = row['trkHit_distRO']
        adcVsDistRO.Fill(DistRO, adc)  
    
    return adcVsDistRO

def RadiusVsDriftTime(df,driftTube_type):
    if (driftTube_type=='sMDT'):
        radiusVsdriftTime =ROOT.TH2D("RadVsDriftTime", "Radius vs. DriftTime", 150, 0, 250, 150, -9,9)
    if (driftTube_type=='MDT'):
        radiusVsdriftTime =ROOT.TH2D("RadVsDriftTime", "Radius vs. DriftTime", 150, 0, 800, 150, -9,9)

    radiusVsdriftTime.GetXaxis().SetTitle("Drift_Time")
    radiusVsdriftTime.GetYaxis().SetTitle("Radius")
    radiusVsdriftTime.GetZaxis().SetTitle("Entries")
    radiusVsdriftTime.SetStats(1)
# Iterate over the DataFrame to fill the histogram
    for index, row in df.iterrows():
        radius = row['trkHit_driftRadius']
        drift_time=row['trkHit_driftTime']
        # print(radius,residual)
        radiusVsdriftTime.Fill(drift_time,radius) 
    return radiusVsdriftTime
    