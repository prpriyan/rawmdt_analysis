import uproot
import pandas as pd
import muonfixedid, chamberlist_run3  , splitter_regions_Run2, mdtCalib_functions 
import awkward as ak

def extract_chamberInfo(inputFile):
    f=uproot.open(inputFile)  
    tree=f['Segments']
    run = inputFile.split('0',1)[1][:6]    #splits the file name string literal and spits our the run number    
    region = int(inputFile.split("-", 1)[1][:4])   #  splits the file name string literal and spits our the region
    lb = inputFile.split("-", 1)[0][-9:] 
    branchList=tree.keys()          # gives the "keys"(columns) of the ROOT datafile                        
    branchPrefix=['trk', 'trkHit', 'event', 'rawMdt']        # created a list of major "keys"/branches => confirm ??
    branchMapping={key : [] for key in branchPrefix}          # creates a dictionary out of the root file with branches as the keys. why??

    for branch in branchList:          #separate out the prefix in keys name and append it to the branchMapping dictionary. why??
        branchName, variableName= branch.split('_', 1) 
        if branchName in branchPrefix:
            branchMapping[branchName].append(branch)   
            
    # convert mdtCalname(expand) to mdtHardnameexpand() by chamberlist.py . why??
    chamberInRegion = [chamberlist_run3.MDThardname(chamberlist_run3.MDTindex(x)) for x in splitter_regions_Run2.regionlist[region]]
    # print('process run %s lumiblock %s region %s, chamberList %s' % (run, lb, region, chamberInRegion))
    return branchMapping, run, lb, region, chamberInRegion

def extracting_dataframe(inputFile, op_chamber):
    f=uproot.open(inputFile)  
    tree=f['Segments']
    BranchMapping,_,_,_,_=extract_chamberInfo(inputFile)
    df_raw=ak.to_dataframe(tree.arrays(BranchMapping['rawMdt']))  #Convert ROOT file to awkwards array => to Pandas dataframe 
    df_trk=ak.to_dataframe(tree.arrays(BranchMapping['trkHit']))

    #expand all raw and trk hits with correct chamberName and tubeInfo
    df_raw['rawMdt_TubeName'] = df_raw['rawMdt_id'].apply(lambda x : chamberlist_run3.MDTtubename(int(x)))
    df_raw['rawMdt_ChamberName'] = df_raw['rawMdt_TubeName'].apply(lambda x :x[:7])
    # df_raw['rawHit_ChamberType'] = df_raw['rawHits_TubeName'].apply(lambda x :x[:3])

    df_trk['trkHit_tubeName'] = df_trk['trkHit_id'].apply(lambda x : chamberlist_run3.MDTtubename(int(x)))
    df_trk['trkHit_ChamberName'] = df_trk['trkHit_tubeName'].apply(lambda x :x[:7])
    # df_trk['trkHit_type'] = df_trk['trkHit_tubeInfo'].apply(lambda x :x[:3])

# Creates new dataframe that only contains data for specific chamber
    df_chamber_raw = df_raw[df_raw['rawMdt_ChamberName']==op_chamber]
    df_chamber_trk= df_trk[df_trk['trkHit_ChamberName']==op_chamber]

    # df_all = tree.arrays() #gives total numner of events and not hits
    # total_evts=len(df_all)
    # print(f'Total Event in this file is {total_evts}')
    return df_chamber_raw, df_chamber_trk

# ***************-------------------------****************************

#Converting tubename to TubeID
#Converts the tubename to an ID=> mostly for df_raw data; df-trk already has a tube_id
def tube_id(row, chamber):
    # Grab the tube name
    tube_name = row['rawHits_TubeName']  # Assuming the column name that holds the tube name is 'rawMdt_tubeInfo'
    # Split the tube name once and unpack into components
    _, ml_index, ly_index, tube_no = tube_name.split('-')
    # Convert string indices to integers
    ml_index = int(ml_index)
    ly_index = int(ly_index)
    tube_no = int(tube_no)

    unique_tube_id = ((ml_index - 1) * chamberlist_run3.MDTnTml(chamber, 1) +
                      (ly_index - 1) * chamberlist_run3.MDTnTly(chamber, ml_index) +
                      tube_no)
    return unique_tube_id


# inputFile="data24_calib.00476785.calibration_MuonAll.daq.RAW.0001_0004-0113.ntuple.root"
# OPERATING_CHMABER="BIS7A02"
# extracting_dataframe("data24_calib.00476785.calibration_MuonAll.daq.RAW.0001_0004-0113.ntuple.root", OPERATING_CHMABER)